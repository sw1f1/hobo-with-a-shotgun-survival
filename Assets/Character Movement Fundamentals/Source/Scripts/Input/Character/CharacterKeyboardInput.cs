﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CMF
{
    public class CharacterKeyboardInput : CharacterInput
    {
		private WeaponController weaponController;

		private void OnEnable() {
			weaponController = GetComponent<WeaponController>();
			InputManager.OnPointerDownAttack += OnPointerDownAttack;
			InputManager.OnPointerUpAttack += OnPointerUpAttack;
			InputManager.OnReloadWeapon += OnReload;
		}

		private void OnDisable() {
			InputManager.OnPointerDownAttack -= OnPointerDownAttack;
			InputManager.OnPointerUpAttack -= OnPointerUpAttack;
			InputManager.OnReloadWeapon -= OnReload;
		}

        public override float GetHorizontalMovementInput()
		{
			return InputManager.GetHorizontalMovementInput();
		}

		public override float GetVerticalMovementInput()
		{
			return InputManager.GetVerticalMovementInput();
		}

        public override bool IsSprintKeyPressed()
        {
            return InputManager.IsSprintKeyPressed();
        }

        public override bool IsJumpKeyPressed()
		{
			return InputManager.IsJumpKeyPressed();
		}

		public override bool IsCrouchKeyPressed()
		{
			return InputManager.IsCrouchKeyPressed();
		}

		public void OnPointerDownAttack()
		{
			weaponController.OnAttack();
		}

		public void OnPointerUpAttack()
		{
			weaponController.OnEndAttck();
		}

		public void OnReload()
		{
			weaponController.OnReload();
		}

    }
}
