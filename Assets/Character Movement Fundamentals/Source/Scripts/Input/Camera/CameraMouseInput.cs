﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CMF
{
    public class CameraMouseInput : CameraInput
    {
	    public override float GetHorizontalCameraInput()
        {
            return InputManager.GetHorizontalCameraInput();
        }

        public override float GetVerticalCameraInput()
        {
            return InputManager.GetVerticalCameraInput();
        }
    }
}
