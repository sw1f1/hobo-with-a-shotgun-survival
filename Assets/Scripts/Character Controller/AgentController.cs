﻿using Scripts.MovementController.States;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts.MovementController
{
    public class AgentController : MonoBehaviour
    {
        public enum StateType {None = -1, Walk, Run, Sprint, Crouch, Jump, Falling, Roll, Climb, Ladder, MoveTo, UsedObject, Idle}
        [SerializeField] private StateType stateType;
        [SerializeField] private Movement idleMovement = new Movement(0, 0, 0);
        [SerializeField] private Movement walkMovement = new Movement(2, 20, 4);
        [SerializeField] private Movement runMovement = new Movement(4, 20, 4);
        [SerializeField] private Movement airMovement = new Movement(4, 20, 0);
        [SerializeField] private Movement ladderMovement = new Movement(1, 20, 0);
        [SerializeField] private Movement sprintMovement = new Movement(6, 20, 4);
        [SerializeField] private Movement crouchMovement = new Movement(1, 20, 4);

        [SerializeField] private Transform target;

        private IInput input;
        private AgentMovement movement;
        private AnimationController animation;
        private WeaponController weapon;
        private State state;
        private State[] states;
        private State lastMovementState;

        public IInput Input => input;
        public AgentMovement Movement => movement;
        public AnimationController Animation => animation;
        public WeaponController Weapon => weapon;

        public StateType TypeState => stateType;
        public State State => state;

        private void Awake()
        {
            input = GetComponent<IInput>();
            movement = GetComponent<AgentMovement>();
            animation = GetComponent<AnimationController>();
            weapon = GetComponent<WeaponController>();

            InitStates();
        }

        private void InitStates()
        {
            states = new State[12];
            states[0] = new WalkState(StateType.Walk, this, walkMovement);
            states[1] = new RunState(StateType.Run, this, runMovement);
            states[2] = new SprintState(StateType.Sprint, this, sprintMovement);
            states[3] = new CrouchState(StateType.Crouch, this, crouchMovement);
            states[4] = new JumpState(StateType.Jump, this, airMovement);
            states[5] = new FallState(StateType.Falling, this, airMovement);
            states[8] = new LadderState(StateType.Ladder, this, ladderMovement, 1);
            states[9] = new MoveToState(StateType.MoveTo, this, runMovement);
            states[10] = new UsedObjectState(StateType.UsedObject, this, walkMovement);
            states[11] = new IdleState(StateType.Idle, this, idleMovement);
        }

        private void Start()
        {
            ChangeState(GetState(StateType.Run));
        }

        public State GetState(StateType state)
        {
            return states[(int)state];
        }

        private void Update()
        {
            if (state == null)
                return;

            state.HandleInput();
            state.Update();

            foreach(var state in states)
            {
                if(state != null)
                    state.UpdateCooldawn();
            }
            UpdateAim();
        }

        private void UpdateAim()
        {
            bool aim = input.IsAimKeyPressed();

            if (aim && TypeState == AgentController.StateType.Run)
            {
                ChangeState(AgentController.StateType.Walk);
            }

            if (!aim && TypeState == AgentController.StateType.Walk)
            {
                ChangeState(AgentController.StateType.Run);
            }
        }

        public void ChangeState(StateType stateType)
        {
            ChangeState(GetState(stateType));
        }

        private void ChangeState(State newState)
        {
            if(state != null)
            {
                state.Exit();

                if (state.Movement.SaveToLastState)
                {
                    lastMovementState = state;
                }
            }

            state = newState;
            stateType = state.Type;

            state.Enter();
        }

        public void ChangeStateOnLast()
        {
            if(lastMovementState != null)
            {
                ChangeState(lastMovementState);
            }else
            {
                ChangeState(StateType.Walk);
            }
        }


        private void OnControllerColliderHit(ControllerColliderHit hit)
        {
            OnColliderHitLadder(hit.collider);
        }

        private void OnTriggerEnter(Collider other)
        {
            OnColliderHitLadder(other);
        }

        private void OnColliderHitLadder(Collider collider)
        {
            if (collider.gameObject.layer == 13 && stateType != StateType.Ladder && GetState(StateType.Ladder).CanUsed) //Ladder
            {
                ChangeState(StateType.Ladder);
                LadderState ladder = state as LadderState;

                Ladder ladderGO = collider.gameObject.GetComponent<Ladder>();
                ladder.SetDirection(ladderGO.Forward);
                ladder.SetPositionOnLadder(ladderGO.Center);
            }
        }

    }

    [System.Serializable]
    public class Movement
    {
        [SerializeField] private float movementSpeed;
        [SerializeField] private float rotationSpeed;
        [SerializeField] private float jumpForce;
        [SerializeField] private bool saveToLastState = false;

        public float MovementSpeed => movementSpeed;
        public float RotationSpeed => rotationSpeed;
        public float JumpForce => jumpForce;
        public bool SaveToLastState => saveToLastState;

        public Movement(float movementSpeed, float rotationSpeed, float jumpForce)
        {
            this.movementSpeed = movementSpeed;
            this.rotationSpeed = rotationSpeed;
            this.jumpForce = jumpForce;
        }
    }
}
