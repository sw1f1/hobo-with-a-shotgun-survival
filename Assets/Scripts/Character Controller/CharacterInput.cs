﻿using Scripts.MovementController.AgentCamera;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts.MovementController
{
    public class CharacterInput : MonoBehaviour, IInput
    {
        private WeaponController weaponController;
        private AgentController agentController;

        private void Awake()
        {
            weaponController = GetComponent<WeaponController>();
            agentController = GetComponent<AgentController>();
        }

        private void OnEnable()
        {
            InputManager.OnPointerDownAttack += OnPointerDownAttack;
            InputManager.OnPointerUpAttack += OnPointerUpAttack;
            InputManager.OnReloadWeapon += OnReload;
        }

        private void OnDisable()
        {
            InputManager.OnPointerDownAttack -= OnPointerDownAttack;
            InputManager.OnPointerUpAttack -= OnPointerUpAttack;
            InputManager.OnReloadWeapon -= OnReload;
        }

        public Vector2 GetMovementInput()
        {
            Vector2 input = new Vector2(InputManager.GetHorizontalMovementInput(), InputManager.GetVerticalMovementInput());
            return input.normalized;
        }

        public Vector3 GetMovementDirectionInput()
        {
            Vector3 cameraForwardDirection = Camera.main.transform.forward;
            Vector3 direction = Vector3.Scale(cameraForwardDirection, (Vector3.right + Vector3.forward));

            Debug.DrawRay(Camera.main.transform.position, cameraForwardDirection * 10, Color.red);
            Debug.DrawRay(Camera.main.transform.position, direction * 10, Color.blue);

            return direction.normalized;
        }

        public bool IsJumpKeyPressed()
        {
            return InputManager.IsJumpKeyPressed();
        }

        public bool IsCrouchKeyPressed()
        {
            return InputManager.IsCrouchKeyPressed() && agentController.State.Movement.SaveToLastState;
        }

        public bool IsSprintKeyPressed()
        {
            return InputManager.IsSprintKeyPressed();
        }

        public bool IsAimKeyPressed()
        {
            bool press = InputManager.IsAimKeyPressed() && weaponController.CanAim();
            if(weaponController.CanAim() && weaponController.CanAimScope())
            {
                EventManager.OnSetActiveScopeAIM?.Invoke(press);
            }

            return press;
        }

        public float GetFow()
        {
            if (weaponController.CanAim())
                return weaponController.GetFow();
            else return 60f;
        }

        public float GetHorizontalCameraInput()
        {
            return InputManager.GetHorizontalCameraInput();
        }

        public float GetVerticalCameraInput()
        {
            return InputManager.GetVerticalCameraInput();
        }

        public void OnPointerDownAttack()
        {
            weaponController.OnAttack();
        }

        public void OnPointerUpAttack()
        {
            weaponController.OnEndAttck();
        }

        public void OnReload()
        {
            weaponController.OnReload();
        }
    }


}