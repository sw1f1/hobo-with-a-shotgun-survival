﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts.MovementController
{
    public class AnimationController : MonoBehaviour
    {
        private IInput input;
        private AgentMovement movement;
        private Animator animator;

        private Vector3 smooth;

        private void Awake()
        {
            input = GetComponent<IInput>();
            movement = GetComponent<AgentMovement>();
            animator = GetComponent<Animator>();
        }

        public void HandleMovement(Vector2 input)
        {
            smooth = Vector3.Lerp(smooth, input, Time.deltaTime * 5f);

            animator.SetFloat("X", smooth.x);
            animator.SetFloat("Z", smooth.y);
        }

        public void HandleMovementY(Vector3 direction)
        {
            animator.SetFloat("VerticalSpeed", direction.y);
        }

        public void HandleGrounded(bool isGrounded)
        {
            SetBool("IsGrounded", isGrounded);
        }

        public void SetBool(string name, bool value)
        {
            animator.SetBool(name, value);
        }

        public void SetTrigger(string name)
        {
            animator.SetTrigger(name);
        }

        public void SetFloat(string name, float value)
        {
            animator.SetFloat(name, value);
        }

        public void SetRuntimeAnimatorController(RuntimeAnimatorController runtimeAnimatorController)
        {
            animator.runtimeAnimatorController = runtimeAnimatorController;
        }
    }
}
