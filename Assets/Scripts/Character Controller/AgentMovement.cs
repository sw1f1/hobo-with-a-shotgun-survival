﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts.MovementController
{
    public class AgentMovement : MonoBehaviour
    {
        [SerializeField] private float gravity = 10f;
        [SerializeField] private bool useGravity = true;

        private Movement movement;
        private CharacterController characterController;
        private Vector3 velocity = Vector3.zero;
        private float movementY = 0;
        private Vector3 direction = Vector3.zero;
        private bool isGrounded = false;

        public Vector3 Velocity => velocity;
        public bool IsGrounded => isGrounded;
        public bool UseGravity { get { return useGravity; }
            set
            {
                useGravity = value;
                if(!value)
                    movementY = 0;
            }
        }

        public CharacterController CharacterController => characterController;

        private void Awake()
        {
            characterController = GetComponent<CharacterController>();
        }

        public void SetMovement(Movement movement)
        {
            this.movement = movement;
        }

        public void HandleMovement(Vector2 input, float magnitude = 1)
        {
            velocity = transform.rotation * new Vector3(input.x, 0, input.y); //transform.forward * input.y + transform.right * input.x;

            if (!isGrounded && useGravity)
            {
                movementY -= gravity * Time.deltaTime;
            }

            velocity *= movement.MovementSpeed * magnitude;
            velocity.y = movementY;
            characterController.Move(velocity * Time.deltaTime);
        }

        public void HandleMovementTo(Vector2 input, float magnitude = 1)
        {
            velocity = new Vector3(input.x, 0, input.y);

            if (!isGrounded && useGravity)
            {
                movementY -= gravity * Time.deltaTime;
            }

            velocity *= movement.MovementSpeed * magnitude;
            velocity.y = movementY;
            characterController.Move(velocity * Time.deltaTime);
        }

        public void HandleMovement2D(Vector2 input)
        {
            velocity = transform.up * input.y + transform.right * input.x;
            velocity *= movement.MovementSpeed;
            characterController.Move(velocity * Time.deltaTime);
        }

        public void OnJump()
        {
            if (isGrounded)
            {
                movementY = movement.JumpForce;
            }
        }

        public void OnLand()
        {
            movementY = 0;
        }

        public void HandleMovementDirection(Vector3 direction)
        {
            this.direction = direction;
            RotateAgent();
        }

        public void RotateAgent()
        {
            if (direction == Vector3.zero)
                return;

            Quaternion rotation = Quaternion.LookRotation(direction, Vector3.up);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * movement.RotationSpeed);
        }

        private void Update()
        {
            isGrounded = OnGrounded();
        }

        public bool OnGrounded()
        {
            RaycastHit hit;
            bool isGrounded = false;
            if(Physics.SphereCast(transform.position + Vector3.up * 0.5f, characterController.radius, Vector3.down, out hit, 0.7f))
            {
                isGrounded = true;
            }

            Debug.DrawRay(transform.position + Vector3.up * 0.5f, Vector3.down * 0.7f, Color.red);

            return isGrounded;
        }

        public bool ChackRaycastCollisionHit(out RaycastHit hit)
        {
            bool value = false;
            if (Physics.Raycast(transform.position + Vector3.up, transform.forward, out hit, 1f))
            {
                value = true;
            }

            Debug.DrawRay(transform.position + Vector3.up, transform.forward * 1f, Color.red);

            return value;
        }

        public void SetSizeCollider(float height, Vector3 center)
        {
            characterController.height = height;
            characterController.center = center;
        }

        public void SetActiveCollision(bool value)
        {
            characterController.detectCollisions = value;
        }
    }
}
