﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts.MovementController
{
    public interface IInput
    {
        Vector2 GetMovementInput();
        Vector3 GetMovementDirectionInput();
        bool IsJumpKeyPressed();
        bool IsCrouchKeyPressed();
        bool IsSprintKeyPressed();
        bool IsAimKeyPressed();
    }
}
