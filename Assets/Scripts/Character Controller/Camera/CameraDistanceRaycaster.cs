﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts.MovementController.AgentCamera
{
    public class CameraDistanceRaycaster : MonoBehaviour
    {
        public enum CastType
        {
            Raycast,
            Spherecast
        }

        [SerializeField] private Transform cameraTransform;
        [SerializeField] private CastType castType;
        [SerializeField] private LayerMask layerMask = ~0;
        [SerializeField] private Collider[] ignoreList;
        [SerializeField] private float minimumDistanceFromObstacles = 0.1f;
        [SerializeField] private float smoothingFactor = 25f;
        [SerializeField] private float spherecastRadius = 0.2f;

        private Transform tr;
        private int ignoreRaycastLayer;
        private int[] ignoreListLayers;
        private float preferredDistance;
        private float currentDistance;

        private Vector3 localCastDirection;

        void Awake()
        {
            tr = transform;

            preferredDistance = (cameraTransform.position - tr.position).magnitude;
            currentDistance = preferredDistance;

            localCastDirection = cameraTransform.position - tr.position;
            localCastDirection.Normalize();

            ignoreListLayers = new int[ignoreList.Length];

            localCastDirection = tr.worldToLocalMatrix * localCastDirection;

            ignoreRaycastLayer = LayerMask.NameToLayer("Ignore Raycast");

            layerMask ^= (1 << ignoreRaycastLayer);
        }

        void LateUpdate()
        {
            if (ignoreListLayers.Length != ignoreList.Length)
            {
                ignoreListLayers = new int[ignoreList.Length];
            }

            for (int i = 0; i < ignoreList.Length; i++)
            {
                ignoreListLayers[i] = ignoreList[i].gameObject.layer;
                ignoreList[i].gameObject.layer = ignoreRaycastLayer;
            }

            float _distance = GetCameraDistance();

            for (int i = 0; i < ignoreList.Length; i++)
            {
                ignoreList[i].gameObject.layer = ignoreListLayers[i];
            }

            currentDistance = Mathf.Lerp(currentDistance, _distance, Time.deltaTime * smoothingFactor);

            Vector3 _direction = tr.localToWorldMatrix * localCastDirection;

            cameraTransform.position = tr.position + _direction * currentDistance;

        }

        float GetCameraDistance()
        {
            RaycastHit _hit;
            Vector3 _direction = tr.localToWorldMatrix * localCastDirection;

            if (castType == CastType.Raycast)
            {
                if (Physics.Raycast(new Ray(tr.position, _direction), out _hit, preferredDistance + minimumDistanceFromObstacles, layerMask, QueryTriggerInteraction.Ignore))
                {
                    if (_hit.distance - minimumDistanceFromObstacles < 0f)
                        return _hit.distance;
                    else
                        return _hit.distance - minimumDistanceFromObstacles;
                }
            }
            else
            {
                if (Physics.SphereCast(new Ray(tr.position, _direction), spherecastRadius, out _hit, preferredDistance, layerMask, QueryTriggerInteraction.Ignore))
                {
                    return _hit.distance;
                }
            }
            return preferredDistance;
        }
    }
}
