﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraAIM : MonoBehaviour
{
    [SerializeField] private Transform aimPivot;
    [SerializeField] private LayerMask mask;
    private void Update() 
    {
        RaycastHit hit;
        if(Physics.Raycast(transform.position, transform.forward, out hit, Mathf.Infinity, mask))
        {
            aimPivot.transform.position = hit.point;
        }
    }
}
