﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts.MovementController.AgentCamera
{
    public class CameraController : MonoBehaviour
    {
        public enum CamType {Defoult, Aim}

        [SerializeField] private CamType cameraType = CamType.Defoult;
        [Range(0f, 90f)]
        [SerializeField] private float upperVerticalLimit = 60f;
        [Range(0f, 90f)]
        [SerializeField] private float lowerVerticalLimit = 60f;

        [SerializeField] private float cameraSpeed = 50f;

        private float currentXAngle = 0f;
        private float currentYAngle = 0f;
        private float oldHorizontalInput = 0f;
        private float oldVerticalInput = 0f;
        private Vector3 facingDirection;
        private Vector3 upwardsDirection;
        private float fow = 60f;
        private bool isCrouch;
        private Vector3 positionMove = new Vector3(0f, -0.6f, 0f);

        private Transform tr;
        private Camera cam;
        private CameraRootPosition rootPosition;
        private CharacterInput input;

        private void Start()
        {
            tr = transform;
            cam = GetComponent<Camera>();
            rootPosition = GetComponentInParent<CameraRootPosition>();
            input = rootPosition.Target.GetComponent<CharacterInput>();

            if (cam == null)
                cam = GetComponentInChildren<Camera>();

            currentXAngle = tr.localRotation.eulerAngles.x;
            currentYAngle = tr.localRotation.eulerAngles.y;

            RotateCamera(0f, 0f);
        }


        void Update()
        {
            HandleCameraRotation();
            bool aim = input.IsAimKeyPressed();
            ChangeAim(aim);
            CameraYPosition(input.IsCrouchKeyPressed());
        }

        private void ChangeAim(bool aim)
        {
            if(aim && cameraType == CamType.Defoult)
            {
                cameraType = CamType.Aim;
                fow = input.GetFow();
            }

            if(!aim && cameraType == CamType.Aim)
            {
                cameraType = CamType.Defoult;
                fow = 60f;
            }

            cam.fieldOfView = Mathf.Lerp(cam.fieldOfView, fow, Time.deltaTime * 10f);
        }

        private void CameraYPosition(bool crouch)
        {
            if(crouch)
            {
                isCrouch = !isCrouch;
                if(isCrouch)
                {
                    positionMove = new Vector3(0f, -1.1f, 0f);
                }
                else
                {
                    positionMove = new Vector3(0f, -0.6f, 0f);
                }
            }

            tr.localPosition = Vector3.Lerp(tr.localPosition, positionMove, Time.deltaTime * 10f);
        }

        protected virtual void HandleCameraRotation()
        {
            if (input == null)
                return;

            float _inputHorizontal = input.GetHorizontalCameraInput();
            float _inputVertical = input.GetVerticalCameraInput();

            RotateCamera(_inputHorizontal, _inputVertical);
        }


        protected void RotateCamera(float _newHorizontalInput, float _newVerticalInput)
        {
            oldHorizontalInput = _newHorizontalInput;
            oldVerticalInput = _newVerticalInput;

            currentXAngle += oldVerticalInput;
            currentYAngle += oldHorizontalInput;

            currentXAngle = Mathf.Clamp(currentXAngle, -upperVerticalLimit, lowerVerticalLimit);

            UpdateRotation();
        }

        protected void UpdateRotation()
        {
            tr.localRotation = Quaternion.Euler(new Vector3(0, currentYAngle, 0));

            facingDirection = tr.forward;
            upwardsDirection = tr.up;

            tr.localRotation = Quaternion.Euler(new Vector3(currentXAngle, currentYAngle, 0));
        }

        public void SetFOV(float _fov)
        {
            if (cam)
                cam.fieldOfView = _fov;
        }

        public void SetRotationAngles(float _xAngle, float _yAngle)
        {
            currentXAngle = _xAngle;
            currentYAngle = _yAngle;

            UpdateRotation();
        }

        public void RotateTowardPosition(Vector3 _position, float _lookSpeed)
        {
            Vector3 _direction = (_position - tr.position);

            RotateTowardDirection(_direction, _lookSpeed);
        }

        public void RotateTowardDirection(Vector3 _direction, float _lookSpeed)
        {
            _direction.Normalize();

            _direction = tr.parent.InverseTransformDirection(_direction);

            Vector3 _currentLookVector = GetAimingDirection();
            _currentLookVector = tr.parent.InverseTransformDirection(_currentLookVector);

            float _xAngleDifference = Vector.GetAngle(new Vector3(0f, _currentLookVector.y, 1f), new Vector3(0f, _direction.y, 1f), Vector3.right);

            _currentLookVector.y = 0f;
            _direction.y = 0f;
            float _yAngleDifference = Vector.GetAngle(_currentLookVector, _direction, Vector3.up);

            Vector2 _currentAngles = new Vector2(currentXAngle, currentYAngle);
            Vector2 _angleDifference = new Vector2(_xAngleDifference, _yAngleDifference);

            float _angleDifferenceMagnitude = _angleDifference.magnitude;
            if (_angleDifferenceMagnitude == 0f)
                return;
            Vector2 _angleDifferenceDirection = _angleDifference / _angleDifferenceMagnitude;

            if (_lookSpeed * Time.deltaTime > _angleDifferenceMagnitude)
            {
                _currentAngles += _angleDifferenceDirection * _angleDifferenceMagnitude;
            }
            else
                _currentAngles += _angleDifferenceDirection * _lookSpeed * Time.deltaTime;

            currentYAngle = _currentAngles.y;

            currentXAngle = Mathf.Clamp(_currentAngles.x, -upperVerticalLimit, lowerVerticalLimit);

            UpdateRotation();
        }

        public float GetCurrentXAngle()
        {
            return currentXAngle;
        }

        public float GetCurrentYAngle()
        {
            return currentYAngle;
        }

        public Vector3 GetFacingDirection()
        {
            return facingDirection;
        }

        public Vector3 GetAimingDirection()
        {
            return tr.forward;
        }

        public Vector3 GetAimDirection()
        {
            return tr.position + tr.forward * 2f;
        }

        public Vector3 GetStrafeDirection()
        {
            return tr.right;
        }

        public Vector3 GetUpDirection()
        {
            return upwardsDirection;
        }
    }
}
