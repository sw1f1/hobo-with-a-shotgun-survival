﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts.MovementController.AgentCamera
{
    public class CameraRootPosition : MonoBehaviour
    {
        [SerializeField] private Transform target;
        [SerializeField] private float lerpSpeed = 100f;

        private Transform tr;
        private Vector3 currentPosition;
        private Vector3 localPositionOffset;
        private Vector3 refVelocity;

        public Transform Target => target;

        private void Awake()
        {
            if (target == null)
                target = this.transform.parent;

            tr = transform;
            tr.SetParent(null);

            currentPosition = transform.position;

            localPositionOffset = tr.localPosition;
        }

        private void OnEnable()
        {
            ResetCurrentPosition();
        }

        private void Update()
        {
            SmoothUpdate();
        }

        private void SmoothUpdate()
        {
            currentPosition = Smooth(currentPosition, target.position, lerpSpeed);
            tr.position = Vector3.Lerp(tr.position, currentPosition, lerpSpeed * Time.deltaTime);
        }

        private Vector3 Smooth(Vector3 _start, Vector3 _target, float _smoothTime)
        {
            Vector3 _offset = tr.localToWorldMatrix * localPositionOffset;
            _target += _offset;
            return Vector3.SmoothDamp(_start, _target, ref refVelocity, 0);
        }

        public void ResetCurrentPosition()
        {
            Vector3 _offset = tr.localToWorldMatrix * localPositionOffset;
            currentPosition = target.position + _offset;
        }
    }
}
