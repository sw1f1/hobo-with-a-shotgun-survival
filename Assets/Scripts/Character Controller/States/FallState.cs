﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Scripts.MovementController.AgentController;

namespace Scripts.MovementController.States
{
    public class FallState : State
    {
        private Vector2 input;
        private Vector3 direction;

        public FallState(StateType state, AgentController agentController, Movement movement) : base(state, agentController, movement)
        {

        }

        public override void Enter()
        {

        }

        public override void Exit()
        {

        }

        public override void HandleInput()
        {
            input = controller.Input.GetMovementInput();
            direction = controller.Input.GetMovementDirectionInput();
        }

        public override void Update()
        {
            controller.Movement.HandleMovement(input);
            controller.Movement.HandleMovementDirection(direction);

            controller.Animation.HandleMovement(input);
            controller.Animation.HandleMovementY(controller.Movement.Velocity);
            controller.Animation.SetBool("IsGrounded", controller.Movement.IsGrounded);

            if (controller.Movement.IsGrounded)
            {
                controller.Movement.OnLand();
                controller.ChangeStateOnLast();
            }
        }
    }
}
