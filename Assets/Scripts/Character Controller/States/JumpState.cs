﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Scripts.MovementController.AgentController;

namespace Scripts.MovementController.States
{
    public class JumpState : State
    {
        private Vector2 input;
        private Vector3 direction;

        public JumpState(StateType state, AgentController agentController, Movement movement) : base(state, agentController, movement)
        {

        }

        public override void Enter()
        {
            controller.Movement.SetMovement(movement);
            controller.Animation.SetTrigger("OnJump");
        }

        public override void Exit()
        {

        }

        public override void HandleInput()
        {
            input = controller.Input.GetMovementInput();
            direction = controller.Input.GetMovementDirectionInput();
        }

        public override void Update()
        {
            controller.Movement.HandleMovement(input);
            controller.Movement.HandleMovementDirection(direction);

            controller.Animation.HandleMovement(input);
            controller.Animation.HandleMovementY(controller.Movement.Velocity);

            if (controller.Movement.Velocity.y < 0 && !controller.Movement.IsGrounded)
            {
                controller.ChangeState(StateType.Falling);
            }

            if(controller.Movement.IsGrounded)
            {
                controller.Animation.SetBool("IsGrounded", controller.Movement.IsGrounded);
                controller.ChangeStateOnLast();
            }
        }
    }
}