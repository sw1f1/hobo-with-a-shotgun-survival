﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Scripts.MovementController.AgentController;

namespace Scripts.MovementController.States
{
    public class SprintState : WalkState
    {
        public SprintState(StateType state, AgentController agentController, Movement movement) : base(state, agentController, movement)
        {

        }

        public override void Enter()
        {
            controller.Animation.SetTrigger("IsRun");
            controller.Movement.SetMovement(movement);
            controller.Weapon.IK.SetActive(false);
        }

        public override void Exit()
        {
            controller.Weapon.IK.SetActive(true);
        }

        public override void HandleInput()
        {
            input = controller.Input.GetMovementInput();
            direction = controller.Input.GetMovementDirectionInput();

            if (controller.Input.IsJumpKeyPressed())
            {
                controller.Movement.OnJump();
                controller.ChangeState(StateType.Jump);
            }

            if (controller.Input.IsCrouchKeyPressed())
            {
                controller.ChangeState(StateType.Crouch);
            }

            if (!controller.Input.IsSprintKeyPressed())
            {
                controller.ChangeStateOnLast();
            }
        }

        public override void Update()
        {
            controller.Movement.HandleMovement(input);
            controller.Movement.HandleMovementDirection(direction);

            if (input.y > 0 && input.x == 0)
            {
                input.y = 2;
            }

            controller.Animation.HandleMovement(input);

            if (!controller.Movement.IsGrounded)
            {
                controller.ChangeState(StateType.Falling);
            }
        }
    }
}

