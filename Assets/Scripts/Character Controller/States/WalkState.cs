﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Scripts.MovementController.AgentController;

namespace Scripts.MovementController.States
{
    public class WalkState : State
    {
        protected Vector2 input;
        protected Vector3 direction;

        public WalkState(StateType state, AgentController agentController, Movement movement) : base(state, agentController, movement)
        {

        }

        public override void Enter()
        {
            controller.Animation.SetTrigger("IsWalk");
            controller.Movement.SetMovement(movement);
        }

        public override void Exit()
        {

        }

        public override void HandleInput()
        {
            input = controller.Input.GetMovementInput();
            direction = controller.Input.GetMovementDirectionInput();

            if(controller.Input.IsJumpKeyPressed())
            {
                controller.Movement.OnJump();
                controller.ChangeState(StateType.Jump);
            }

            if(controller.Input.IsCrouchKeyPressed())
            {
                controller.ChangeState(StateType.Crouch);
            }

            if (controller.Input.IsSprintKeyPressed())
            {
                controller.ChangeState(StateType.Sprint);
            }
        }

        public override void Update()
        {
            controller.Movement.HandleMovement(input);
            controller.Movement.HandleMovementDirection(direction);

            controller.Animation.HandleMovement(input);

            if (!controller.Movement.IsGrounded)
            {
                controller.ChangeState(StateType.Falling);
            }
        }
    }
}
