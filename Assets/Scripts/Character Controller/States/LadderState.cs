﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Scripts.MovementController.AgentController;

namespace Scripts.MovementController.States
{
    public class LadderState : State
    {
        protected Vector2 input;
        protected Vector3 direction;
        protected Vector3 positionOnLadder = Vector3.zero;

        public LadderState(StateType state, AgentController agentController, Movement movement, float cooldawn) : base(state, agentController, movement, cooldawn)
        {

        }

        public override void Enter()
        {
            controller.Animation.SetTrigger("OnLadder");
            controller.Animation.SetBool("IsLadder", true);
            controller.Movement.SetMovement(movement);
            controller.Movement.UseGravity = false;
            controller.Weapon.IK.SetActive(false);

        }

        public override void Exit()
        {
            controller.Animation.SetBool("IsGrounded", true);
            controller.Animation.SetBool("IsLadder", false);
            controller.Movement.UseGravity = true;
            controller.Weapon.IK.SetActive(true);
            timer = cooldawn;
        }

        public override void HandleInput()
        {
            input = controller.Input.GetMovementInput();

            if (controller.Input.IsJumpKeyPressed())
            {
                controller.Movement.OnJump();
                controller.ChangeState(StateType.Jump);
            }
        }

        public override void Update()
        {
            input.x = 0;
            RaycastHit hit;
            bool isLadder = controller.Movement.ChackRaycastCollisionHit(out hit);

            if (!isLadder && input.y > 0)
            {
                controller.Movement.HandleMovement(input, 3f);
                input *= 0.1f;
            }
            controller.Movement.HandleMovement2D(input);

            controller.Movement.HandleMovementDirection(direction);

            controller.Animation.HandleMovement(input);

            if(!isLadder && controller.Movement.IsGrounded && input.y > 0)
            {
                controller.ChangeStateOnLast();
            }

            if (isLadder && !hit.collider.isTrigger && controller.Movement.IsGrounded && input.y < 0)
            {
                controller.ChangeStateOnLast();
            }

            Vector3 dir = positionOnLadder - controller.transform.position;
            controller.Movement.HandleMovementTo(new Vector2(dir.x, dir.z), 3f);
        }

        public void SetDirection(Vector3 direction)
        {
            this.direction = direction;
        }

        public void SetPositionOnLadder(Vector3 position)
        {
            this.positionOnLadder = position;
        }
    }
}