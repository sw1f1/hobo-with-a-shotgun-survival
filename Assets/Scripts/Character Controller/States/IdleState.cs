﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Scripts.MovementController.AgentController;

namespace Scripts.MovementController.States
{
    public class IdleState : State
    {
        protected Vector2 input = Vector3.zero;
        protected Vector3 direction;

        public IdleState(StateType state, AgentController agentController, Movement movement) : base(state, agentController, movement)
        {

        }

        public override void Enter()
        {
            controller.Animation.SetTrigger("IsRun");
            controller.Movement.SetMovement(movement);
        }

        public override void Exit()
        {

        }

        public override void HandleInput()
        {
            direction = controller.Input.GetMovementDirectionInput();
        }

        public override void Update()
        {
            controller.Movement.HandleMovement(input);
            //controller.Movement.HandleMovementDirection(direction);

            controller.Animation.HandleMovement(input);

            if (!controller.Movement.IsGrounded)
            {
                controller.ChangeState(StateType.Falling);
            }
        }
    }
}
