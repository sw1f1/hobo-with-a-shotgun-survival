﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Scripts.MovementController.AgentController;

namespace Scripts.MovementController.States
{
    public abstract class State
    {
        protected StateType stateType;
        protected AgentController controller;
        protected Movement movement;

        protected float cooldawn;
        protected float timer;

        public Movement Movement => movement;
        public bool CanUsed => timer == 0;

        public State(StateType stateType, AgentController agentController, Movement movement, float cooldawn = 0)
        {
            this.stateType = stateType;
            this.controller = agentController;
            this.movement = movement;
            this.cooldawn = cooldawn;
        }

        public StateType Type => stateType;

        public abstract void Enter();
        public abstract void HandleInput();
        public abstract void Update();
        public abstract void Exit();

        public void UpdateCooldawn()
        {
            if (timer == 0)
                return;

            timer -= Time.deltaTime;
            if(timer <= 0)
            {
                timer = 0;
            }
        }
    }
}
