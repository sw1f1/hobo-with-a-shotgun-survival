﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Scripts.MovementController.AgentController;

namespace Scripts.MovementController.States
{
    public class CrouchState : WalkState
    {
        public CrouchState(StateType state, AgentController agentController, Movement movement) : base(state, agentController, movement)
        {

        }

        public override void Enter()
        {
            controller.Animation.SetBool("OnCrouch", true);
            controller.Movement.SetMovement(movement);
            controller.Movement.SetSizeCollider(1.2f, new Vector3(0, 0.6f, 0));
        }

        public override void Exit()
        {
            controller.Animation.SetBool("OnCrouch", false);
            controller.Movement.SetSizeCollider(1.6f, new Vector3(0, 0.8f, 0));
        }

        public override void HandleInput()
        {
            input = controller.Input.GetMovementInput();
            direction = controller.Input.GetMovementDirectionInput();

            if (controller.Input.IsJumpKeyPressed())
            {
                controller.Movement.OnJump();
                controller.ChangeState(StateType.Jump);
            }

            if (controller.Input.IsCrouchKeyPressed())
            {
                controller.ChangeState(StateType.Run);
            }
        }

        public override void Update()
        {
            base.Update();
        }
    }
}

