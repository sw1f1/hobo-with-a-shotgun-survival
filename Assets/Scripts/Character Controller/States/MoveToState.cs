﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Scripts.MovementController.AgentController;

namespace Scripts.MovementController.States
{
    public class MoveToState : State
    {
        protected Vector2 input = Vector3.zero;
        protected Vector3 direction = Vector3.zero;
        protected Transform target;

        public MoveToState(StateType state, AgentController agentController, Movement movement) : base(state, agentController, movement)
        {

        }

        public override void Enter()
        {
            controller.Animation.SetTrigger("IsRun");
            controller.Movement.SetMovement(movement);
        }

        public override void Exit()
        {

        }

        public override void HandleInput()
        {
            if (target == null)
                return;

            Vector3 dir = target.position - controller.transform.position;
            input = new Vector2(dir.x, dir.z);

            if(dir.magnitude > 1f)
                input = input.normalized;

            direction = dir.normalized;
            direction = Vector3.Scale(direction, (Vector3.right + Vector3.forward));
        }

        public override void Update()
        {
            if (target == null)
                return;

            controller.Movement.HandleMovementTo(input);
            controller.Movement.HandleMovementDirection(direction);

            controller.Animation.HandleMovement(input);

            if (!controller.Movement.IsGrounded)
            {
                controller.ChangeState(StateType.Falling);
            }
        }

        public void SetTarget(Transform target)
        {
            this.target = target;
        }
    }
}