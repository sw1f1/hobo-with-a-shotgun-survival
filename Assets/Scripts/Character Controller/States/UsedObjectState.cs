﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Scripts.MovementController.AgentController;

namespace Scripts.MovementController.States
{
    public class UsedObjectState : State
    {
        private IUsable usable;
        private float time;
        public UsedObjectState(StateType state, AgentController agentController, Movement movement) : base(state, agentController, movement)
        {

        }

        public override void Enter()
        {
            controller.Animation.SetBool("IsUse", true);
            controller.Animation.SetTrigger("Use");
            controller.Weapon.IK.SetActive(false);
        }

        public override void Exit()
        {
            controller.Animation.SetBool("IsUse", false);
            controller.Weapon.IK.SetActive(true);
        }

        public override void HandleInput()
        {

        }

        public override void Update()
        {
            time -= Time.deltaTime;
            if(time <= 0)
            {
                usable.OnUse();
                controller.ChangeStateOnLast();
            }
        }

        public void SetUsableObject(IUsable usable)
        {
            this.usable = usable;
            this.time = usable.TimeUsed;
        }
    }
}
