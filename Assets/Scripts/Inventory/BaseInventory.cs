﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Scripts.Managers;
using Scripts.Items;
using static Scripts.Items.Item;

public class BaseInventory : MonoBehaviour
{
    [SerializeField] protected string nameShow;
    [SerializeField] protected bool combineAll = false;
    [SerializeField] protected int maxWeigth;
    
	[SerializeField] protected List<FastItem> items = new List<FastItem>();

    public List<FastItem> Items => items;
    public int MaxWeigth => maxWeigth;
    public string Name => nameShow;

	public float Weigth()
	{
		float num = 0f;
		foreach (FastItem item in items)
		{
			num += ItemManager.GetItem(item).Weigth * item.Count;
		}
		return num;
	}

	public virtual void Reset()
	{
		items.Clear();
	}

    public virtual void Start()
    {

    }

    public virtual void UpdateState()
    {

    }

    public virtual bool AddItem(FastItem item, bool combine = true)
	{
		if (item.Equals(FastItem.None))
		{
			return false;
		}

 		if (!combine)
			{
				items.Add(item.Clone());
			}
			else
			{
				AddItemCombine(item);
			}

		return true;
	}

	private void AddItemCombine(FastItem item)
	{
		if (ItemManager.GetItem(item).Combine || combineAll)
		{
            var findItems = items.Where(x=> x.ID == item.ID);
            if(findItems.Count() > 0)
            {
                FastItem findItem = findItems.FirstOrDefault();
                findItem.Count += item.Count;
                return;
            }
		}

        items.Add(item.Clone());
	}

	public bool DeleteItem(FastItem item)
	{
        if (items.Contains(item))
		{
            items.Remove(item);
            return true;
		}
		return false;
	}

	public FastItem FindAmmo(Ammo ammo)
	{
		FastItem fammo = FastItem.None;

		var findItems = items.Where(x=> x.ID == ammo.GetFastItem().ID);
		if(findItems.Count() > 0)
		{
			fammo = findItems.FirstOrDefault();
		}
		return fammo;
	}

    public FastItem[] FindItemOnType(TypeItem typeItem)
    {
        var findItems = items.Where(x => x.Type == (int)typeItem);
        return findItems.ToArray();
    }


    public void Clear()
	{
		items.Clear();
	}
}
