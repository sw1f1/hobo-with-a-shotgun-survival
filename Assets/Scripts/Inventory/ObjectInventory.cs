using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Scripts.Utility;
using Scripts.Managers;

public class ObjectInventory : BaseInventory, IUsable
{
    [SerializeField] private bool combineWithOther;
    [SerializeField] private float timeWaitOpen = 1f;
    [SerializeField] private string textUse;
    [SerializeField] private float timeUsed = 1f;

    public Action<IUsable> OnUsed { get; set; }

    private bool isActive = true;
    public bool IsActive => isActive;

    public bool CombineWithOther {get {return combineWithOther;} set {combineWithOther = value;}}
    public Transform Transform {get {return transform;}}
    public string TextUse => textUse;
    public float TimeUsed => timeUsed;

    private Renderer render;
    private bool used = false;

    public override void Start()
	{
        base.Start();
        render = GetComponentInChildren<Renderer>();
        Show(false);     
    }

	public void Show(bool value)
	{
        render.sharedMaterial.SetInt("OUTLINES", 1);
	}

	public void OnClickUse()
	{
        if (used)
            return;

        EventManager.OnVisibleUseObject?.Invoke(false, null);

        used = true;
    }

    public void OnUse()
    {
        InputManager.OpenOtherInventory(this);
        timeUsed = 0;
        used = false;
    }
}
