using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Scripts.Items;
using System.Linq;
using Scripts.Managers;
public class PlayerInventory : BaseInventory
{
    public Action<int, FastItem> OnUpdateEquipItem;
    public Action<int> OnSetCurrentWeapon;

    private static int EQUIPMENT_LENGTH = 7;
    [SerializeField] private int defoltWeight = 5;
	[SerializeField] private int currentWeapon;
	[SerializeField] private Item.TypeItem[] equipmentType;
    [SerializeField] private Clothes.TypeClothes[] equipmentTypeClothes;
    [SerializeField] private FastItem[] equipment;

    public int DefoltWeight => defoltWeight;

	
	public void SetEquipItem(int index, FastItem item)
	{
        FastItem lastItem = equipment[index];
        if(lastItem != null && lastItem != FastItem.None)
        {
            //AddItem(lastItem);
        }

        equipment[index] = item;
        OnUpdateEquipItem?.Invoke(index, item);

        if (index == 3)
        {
            return;
        }

        if(equipmentType[index] == Item.TypeItem.Food)
        {
            return;
        }

		if(equipmentType[index] == Item.TypeItem.Weapon)
		{
            OnSetCurrentWeapon?.Invoke(index);
            SetWeapon(index, item as FastItemWeapon);
			return;
		}

        if (equipmentType[index] == Item.TypeItem.Clothes)
        {
            SetClothes(index, item as FastItemClothes);
            return;
        }
    }

    public override bool AddItem(FastItem item, bool combine = true)
    { 
        if (!item.Equals(FastItem.None) && ItemManager.GetItem(item).Combine)
        {
            var findItems = equipment.Where(x => x.ID == item.ID);
            if (findItems.Count() > 0 && !findItems.FirstOrDefault().Equals(item))
            {
                FastItem findItem = findItems.FirstOrDefault();
                findItem.Count += item.Count;
                UpdateEquipment();
                return true;
            }
        }

        return base.AddItem(item, combine);
    }

    private void SetWeapon(int index, FastItemWeapon item)
	{
		PlayerStats.PlayerGO.GetComponent<WeaponController>().SetWeapon(item, this);
	}

    private void SetClothes(int index, FastItemClothes item)
    {
        PlayerStats.PlayerGO.GetComponent<ClothesController>().SetClothes(item, equipmentTypeClothes[index - 4]);
    }

	public FastItem GetEquipItem(int index)
	{
		return equipment[index];
	}

    public void SwitchCurentWeapon()
	{
		currentWeapon++;
		if (currentWeapon > 1)
		{
			currentWeapon = 0;
		}

		SetEquipItem(currentWeapon, equipment[currentWeapon]);
	}

    public void SwitchWeapon(int index)
    {
        currentWeapon = index;
        SetEquipItem(currentWeapon, equipment[currentWeapon]);
    }

    public FastItem GetCurentWeapon()
	{
		return equipment[currentWeapon];
	}

    public FastItem GetWeapon(int index)
    {
        return equipment[index];
    }

    public FastItem GetUnCurentWeapon()
	{
		int num = currentWeapon + 1;
		if (num > 1)
		{
			num = 0;
		}
		return equipment[num];
	}

	public override void Reset()
	{
		maxWeigth = DefoltWeight;

		base.Reset();
	}

	public void UpdateEquipment()
	{
		for (int i = 0; i < EQUIPMENT_LENGTH; i++)
		{
            if(equipment[i].Count <= 0)
            {
                SetEquipItem(i, FastItem.None);
            }else
            {
                SetEquipItem(i, equipment[i]);
            }
		}
	}

	public bool EquipmentItem(FastItem fitem)
	{
		for (int i = 0; i < EQUIPMENT_LENGTH; i++)
		{
			if (equipment[i].Equals(fitem))
			{
				return true;
			}
		}
		return false;
	}

	public void DeleteEquipment(FastItem fitem)
	{
		for (int i = 0; i < EQUIPMENT_LENGTH; i++)
		{
			if (equipment[i].Equals(fitem))
			{
				SetEquipItem(i, FastItem.None);
			}
		}
	}

	public bool ContainerEquipment(FastItem fitem)
	{
		for (int i = 0; i < EQUIPMENT_LENGTH; i++)
		{
			if (equipment[i].Equals(fitem))
			{
				return true;
			}
		}
		return false;
	}

    public void UseEqupmentFood()
    {
        if (equipment[2] == null || equipment[2] == FastItem.None)
            return;

        equipment[2].Count -= 1;

        Food food = ItemManager.GetItem(equipment[2]) as Food;
        food.Use(PlayerStats.PlayerGO.GetComponent<HealthController>());

        if (equipment[2].Count == 0)
        {
            DeleteEquipment(equipment[2]);
        }
        else
        {
            OnUpdateEquipItem?.Invoke(2, equipment[2]);
        }

    }

    public override void UpdateState()
    {
        UpdateEquipment();
    }
}
