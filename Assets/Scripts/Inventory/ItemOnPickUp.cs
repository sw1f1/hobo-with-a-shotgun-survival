﻿using Scripts;
using Scripts.Items;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Scripts.MovementController;
using System;

public class ItemOnPickUp : MonoBehaviour, IUsable
{
    [SerializeField] private bool combineWithOther;
    [SerializeField] private string textUse;
    [SerializeField] private float timeUsed = 1f;

    private bool isActive = true;
    public bool IsActive => isActive;

    public bool CombineWithOther { get { return combineWithOther; } set { combineWithOther = value; } }
    public Transform Transform { get { return transform; } }
    public string TextUse => textUse;
    public float TimeUsed => timeUsed;

    public Action<IUsable> OnUsed { get; set; }

    private Renderer render;
    private ItemContainer container;
    private bool used = false;

    public void Start()
    {
        container = GetComponent<ItemContainer>();
        render = GetComponentInChildren<Renderer>();
        Show(false);
    }

    public void Show(bool value)
    {
        render.sharedMaterial.SetInt("OUTLINES", 1);
    }

    public void OnClickUse()
    {
        if (used)
            return;

        EventManager.OnVisibleUseObject?.Invoke(false, null);
        used = true;
    }

    public void OnUse()
    {
        InputManager.OnTakeItem(container.FItem);

        OnUsed?.Invoke(this);

        used = false;
        Destroy(gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        gameObject.layer = 0;
    }
}
