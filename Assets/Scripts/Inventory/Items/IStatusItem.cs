﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IStatusItem 
{
    float Status { get;}
}
