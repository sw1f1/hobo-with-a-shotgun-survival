﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts.Items
{
    [CreateAssetMenu(fileName = "New Build Data", menuName = "Items/Build Data", order = 6)]
    public class Build : Item
    {
        public enum Direction {Down, Up, Left, Right}
        public enum Layers { Floor, Wall, Object }

        [SerializeField] private Layers layer;
        [SerializeField] private bool canFloor = false;
        [SerializeField] private int maxLevel = 2;
        [SerializeField] private int width = 1;
        [SerializeField] private int height = 1;

        public Layers Layer { get { return layer; } set { layer = value; } }
        public bool CanFloor { get { return canFloor; } set { canFloor = value; } }
        public int MaxLevel { get { return maxLevel; } set { maxLevel = value; } }
        public int Width { get { return width; } set { width = value; } }
        public int Height { get { return height; } set { height = value; } }

        public static Direction GetNextDirection(Vector2 scroll, Direction direction)
        {
            if(scroll.y > 0)
                switch(direction)
                {
                    default:
                    case Direction.Down: return Direction.Left;
                    case Direction.Left: return Direction.Right;
                    case Direction.Right: return Direction.Up;
                    case Direction.Up: return Direction.Down;
                }
            else
                switch (direction)
                {
                    default:
                    case Direction.Down: return Direction.Up;
                    case Direction.Up: return Direction.Right;
                    case Direction.Right: return Direction.Left;
                    case Direction.Left: return Direction.Down;
                }
        }
        public static Quaternion GetRotationAngle(Direction direction)
        {
            switch (direction)
            {
                default:
                case Direction.Down: return Quaternion.Euler(0, 0, 0);
                case Direction.Left: return Quaternion.Euler(0, 90, 0);
                case Direction.Right: return Quaternion.Euler(0, 180, 0);
                case Direction.Up: return Quaternion.Euler(0, 270, 0);
            }
        }

        public Vector3Int GetRotationOffset(Direction direction)
        {
            switch (direction)
            {
                default:
                case Direction.Down: return new Vector3Int(0, 0, 0);
                case Direction.Left: return new Vector3Int(0, 0, width);
                case Direction.Right: return new Vector3Int(width, 0, 0);
                case Direction.Up: return new Vector3Int(height, 0, 0);
            }
        }

        public List<Vector2Int> GetGridPositions(Vector2Int offset, Direction direction)
        {
            List<Vector2Int> positions = new List<Vector2Int>();
            switch(direction)
            {
                default:
                case Direction.Down:
                case Direction.Up:
                   for(int x = 0; x < width; x++)
                        for(int y = 0; y < height; y++)
                        {
                            positions.Add(offset + new Vector2Int(x, y));
                        }
                    break;
                case Direction.Left:
                case Direction.Right:
                   for (int x = 0; x < height; x++)
                        for (int y = 0; y < width; y++)
                        {
                            positions.Add(offset + new Vector2Int(x, y));
                        }
                    break;
            }
            return positions;
        }


    }
}
