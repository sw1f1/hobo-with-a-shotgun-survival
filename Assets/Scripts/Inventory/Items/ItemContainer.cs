﻿using Scripts.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts.Items
{
    public class ItemContainer : MonoBehaviour
    {
        [SerializeField] private FastItem fitem = FastItem.None;
        [SerializeField] private Item item;

        private Rigidbody rb;
        private Collider coll;

        public FastItem FItem => fitem;

        private void Awake()
        {
            rb = GetComponent<Rigidbody>();
            coll = GetComponent<Collider>();
        }

        private void Start()
        {
            if(fitem.Equals(FastItem.None) && item != null)
            {
                fitem = item.GetFastItem();
            }
        }

        public void Init(FastItem fitem)
        {
            this.fitem = fitem;
        }

        public void Drop(Vector3 force)
        {
            coll.enabled = true;
            rb.isKinematic = false;
            rb.AddForce(force, ForceMode.Impulse);
        }

        public void Take()
        {
            coll.enabled = false;
            rb.isKinematic = true;
        }
    }
}
