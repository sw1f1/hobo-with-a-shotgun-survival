﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts.Items
{
    [CreateAssetMenu(fileName = "New Food Data", menuName = "Items/Food Data", order = 4)]
    public class Food : Item
    {
        [Header("Food")]
        [SerializeField] private float addHealth;
        [SerializeField] private float addFood;
        [SerializeField] private float addWater;

        public float AddHealth { get { return addHealth; } set { addHealth = value; } }
        public float AddFood { get { return addFood; } set { addFood = value; } }
        public float AddWater { get { return addWater; } set { addWater = value; } }

        public void Use(HealthController healthController)
        {
            healthController.Health.AddValue(addHealth);
            if(healthController as PlayerHealthController)
            {
                PlayerHealthController playerHealthController = healthController as PlayerHealthController;
                playerHealthController.Water.AddValue(addWater);
                playerHealthController.Food.AddValue(addFood);
            }
        }
    }
}
