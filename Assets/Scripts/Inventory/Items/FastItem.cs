﻿using System;
using UnityEngine;
using Scripts.Items;

[System.Serializable]
public class FastItem
{
    public static FastItem None = new FastItem(0,0,0,0);
    [SerializeField] protected int id;
    [SerializeField] protected int type;
    [SerializeField] protected int count;
    
    [SerializeField] protected int indexRnd;

    public int ID => id;
    public int Type => type;
    public int Count {get {return count;} set {count = value;}}

    public int IndexRnd => indexRnd;

    public FastItem(int id, int type)
    {
        this.id = id;
        this.type = type;
        count = 1;
        CreateIndexRnd();
    }

    public FastItem(int id, int type, int count)
    {
        this.id = id;
        this.type = type;
        this.count = count;
        CreateIndexRnd();
    }

    public FastItem(int id, int type, int count, int identificator)
    {
        this.id = id;
        this.type = type;
        this.count = count;
        this.indexRnd = identificator;
    }

    public override bool Equals(object obj)
    {
        if (obj == null)
            return false;

        if(obj is FastItem item)
        {
            return item.id == id && item.type == type && item.count == count && item.indexRnd == indexRnd;
        }

        return false;

    }

    public override int GetHashCode()
    {
        return Tuple.Create(id, type, count).GetHashCode();
    }

    public virtual FastItem Clone()
    {
        return new FastItem(id, type, count);
    }

    public override string ToString()
    {
        return "ID: " + id + ", " + "Type: " + (Item.TypeItem)type + ", " + "Count: " + count + ", " + "Rnd: " + indexRnd + "Type int: " + type;
    }

    public void CreateIndexRnd()
    {
        indexRnd = Tuple.Create(id, type, count, UnityEngine.Random.Range(-1000f, 1000f)).GetHashCode();
    }
}
