﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using Scripts.Utility;
namespace Scripts.Items
{
    public abstract class Item : ScriptableObject
    {
        public enum TypeItem {None = -1, Weapon, Food, Ammo, Clothes, Other, Health, Build }

        [Header("Item Information")]
        [SerializeField] private string nameItem;
        [SerializeField] private string description;
        [SerializeField] protected TypeItem type;
        [SerializeField] private Sprite sprite;
        [SerializeField] private GameObject go;

        [Range(1, 1000)]
        [SerializeField] private float maxCount = 1;
        [SerializeField] private Count countSpawn = new Count(1,1);
        [SerializeField] private float weigth = 1;
        [SerializeField] private bool combine = false;
        [SerializeField] private bool isTrowable;
        [SerializeField] private bool visibleStatus;

        public string Name { get { return nameItem; } set { nameItem = value; } }
        public string Description { get { return description; } set { description = value; } }
        public TypeItem Type { get { return type; } set { type = value; } }
        public Sprite Icon { get { return sprite; } set { sprite = value; } }
        public GameObject Go { get { return go; } set { go = value; } }

        public float MaxCount { get { return maxCount; } set { maxCount = value; } }
        public float Weigth { get { return weigth; } set { weigth = value; } }
        public Count CountSpawn { get { return countSpawn; } set { countSpawn = value; } }
        public bool Combine { get { return combine; } set { combine = value; } }
        public bool IsTrowable { get { return isTrowable; } set { isTrowable = value; } }
        public bool VisibleStatus { get { return visibleStatus; } set { visibleStatus = value; } }

        public int GetCountSpawn => countSpawn.GetInt();

        public FastItem GetFastItem()
        {
            return GetFastItem(GetCountSpawn);
        }

        public virtual FastItem GetFastItem(int count)
        {
            return new FastItem(GetHashCode(), (int)type, count);
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            if (obj is Item item)
            {
                return item.nameItem == nameItem && item.type == type;
            }

            return false;

        }

        public override int GetHashCode()
        {
            return Tuple.Create(nameItem, type).GetHashCode();
        }

    }
}
