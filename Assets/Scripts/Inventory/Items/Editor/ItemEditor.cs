﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Scripts.Utility;

#if UNITY_EDITOR
namespace Scripts.Items
{
    [CustomEditor(typeof(Item)), CanEditMultipleObjects]
    public class ItemEditor : Editor
    {
        bool preview = false;
        SerializedProperty countSpawnProp;

        protected virtual void OnEnable()
        {
            countSpawnProp = serializedObject.FindProperty("countSpawn");
        }

        public override void OnPreviewGUI(Rect r, GUIStyle background)
        {
            Item item = (Item)target;

            if (item.Icon == null) return;

            GUI.DrawTexture(r, item.Icon.texture, ScaleMode.ScaleToFit);
        }

        public override bool HasPreviewGUI()
        {
            return true;
        }

        public override Texture2D RenderStaticPreview(string assetPath, Object[] subAssets, int width, int height)
        {
            Item item = (Item)target;

            if (item.Icon == null) return null;

            Texture2D tex = new Texture2D(width, height);
            EditorUtility.CopySerialized(item.Icon.texture, tex);

            return tex;
        }

        public override void OnInspectorGUI()
        {
            //base.OnInspectorGUI();

            Item item = (Item)target;

            item.Name = EditorGUILayout.TextField("Name", item.Name);
            item.Description = EditorGUILayout.TextField("Description", item.Description);
            item.Type = (Item.TypeItem)EditorGUILayout.EnumPopup("Type", item.Type);

            item.Icon = (Sprite)EditorGUILayout.ObjectField("Icon", item.Icon, typeof(Sprite), true);

            item.Go = (GameObject)EditorGUILayout.ObjectField("Prefab", item.Go, typeof(GameObject), true);

            item.MaxCount = EditorGUILayout.FloatField("MaxCount", item.MaxCount);
            item.Weigth = EditorGUILayout.FloatField("Weigth", item.Weigth);

            EditorGUILayout.PropertyField(countSpawnProp, new GUIContent("Count Spawn"));

            item.Combine = EditorGUILayout.Toggle("Combine", item.Combine);
            item.IsTrowable = EditorGUILayout.Toggle("IsTrowable", item.IsTrowable);
            item.VisibleStatus = EditorGUILayout.Toggle("VisibleStatus", item.VisibleStatus);

            preview = EditorGUILayout.Foldout(preview, "Preview Fast Item");
            if (preview)
            {
                EditorGUILayout.DoubleField("Hash Code", item.GetFastItem().ID);
                if (GUILayout.Button("Calculate Hash Code"))
                {
                    FastItem fastItem = item.GetFastItem();
                    Debug.Log(fastItem.ToString());
                }
            }

            GUILayout.Space(20);

            serializedObject.ApplyModifiedProperties();
        }
    }

    [CustomEditor(typeof(Weapon)), CanEditMultipleObjects]
    public class WeaponEditor : ItemEditor
    {
        bool attackType = false;
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            EditorGUILayout.LabelField("Weapon", EditorStyles.boldLabel);
            Weapon item = (Weapon)target;

            item.Type = Item.TypeItem.Weapon;
            item.TypeWeaponAttack = (Weapon.TypeWeapon)EditorGUILayout.EnumPopup("Type Weapon", item.TypeWeaponAttack);
            item.AnimatorController = (RuntimeAnimatorController)EditorGUILayout.ObjectField("Animator Controller", item.AnimatorController, typeof(RuntimeAnimatorController), true);

            attackType = EditorGUILayout.Foldout(attackType, "Attack Type");
            if (attackType)
            {
                item.AttackType.min = EditorGUILayout.FloatField("Min", item.AttackType.min);
                item.AttackType.max = EditorGUILayout.FloatField("Max", item.AttackType.max);
                item.AttackType.infinity = EditorGUILayout.Toggle("Infinity", item.AttackType.infinity);
            }

            item.Setting.Damage = EditorGUILayout.FloatField("Damage", item.Setting.Damage);

            GUILayout.Space(5);
            if (item.TypeWeaponAttack == Weapon.TypeWeapon.Melee)
            {
                item.TypeMelee = (Weapon.TypeMeleeWeapon)EditorGUILayout.EnumPopup("Type Melee", item.TypeMelee);
            }
            else
            {
                EditorGUILayout.LabelField("Shoot", EditorStyles.boldLabel);
                item.Setting.ClipSize = EditorGUILayout.IntField("Clip Size", item.Setting.ClipSize);
                item.Setting.WeaponAIMMultiply = EditorGUILayout.FloatField("Weapon AIM Multiply", item.Setting.WeaponAIMMultiply);

                item.Setting.TypeAmmo = (Ammo.TypeAmmo)EditorGUILayout.EnumPopup("Type Ammo", item.Setting.TypeAmmo);
                item.Setting.SpeedBullet = EditorGUILayout.FloatField("Speed Bullet", item.Setting.SpeedBullet);
                item.Setting.SpeedAnimationShoot = EditorGUILayout.FloatField("Speed Animation Shoot", item.Setting.SpeedAnimationShoot);
                item.Setting.CountBulletOnShot = EditorGUILayout.IntField("Count Bullet OnShot", item.Setting.CountBulletOnShot);
                item.Setting.CooldownBulletShot = EditorGUILayout.FloatField("Cooldown Bullet Shot", item.Setting.CooldownBulletShot);
                item.Setting.ScatterOffsetOnShoot = EditorGUILayout.FloatField("Scatter Offset OnShoot", item.Setting.ScatterOffsetOnShoot);
                item.Setting.MaxScatterOffsetOnShoot = EditorGUILayout.FloatField("Max Scatter Offset OnShoot", item.Setting.MaxScatterOffsetOnShoot);
                item.Setting.ScopeAIM = EditorGUILayout.Toggle("Scope AIM", item.Setting.ScopeAIM);
                item.Setting.AimFow = EditorGUILayout.FloatField("Aim Fow", item.Setting.AimFow);
            }

            EditorUtility.SetDirty(item);
        }
    }

    [CustomEditor(typeof(Clothes)), CanEditMultipleObjects]
    public class ClothesEditor : ItemEditor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            EditorGUILayout.LabelField("Clothes", EditorStyles.boldLabel);
            Clothes item = (Clothes)target;
            item.Type = Item.TypeItem.Clothes;
            item.ClothesType = (Clothes.TypeClothes)EditorGUILayout.EnumPopup("Clothes Type", item.ClothesType);

            EditorUtility.SetDirty(item);
        }
    }

    [CustomEditor(typeof(Ammo)), CanEditMultipleObjects]
    public class AmmoEditor : ItemEditor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            EditorGUILayout.LabelField("Ammo", EditorStyles.boldLabel);
            Ammo item = (Ammo)target;
            item.Type = Item.TypeItem.Ammo;
            item.AmmoType = (Ammo.TypeAmmo)EditorGUILayout.EnumPopup("Ammo Type", item.AmmoType);
            item.Bullet = (GameObject)EditorGUILayout.ObjectField("Bullet Prefab", item.Bullet, typeof(GameObject), true);

            EditorUtility.SetDirty(item);
        }
    }

    [CustomEditor(typeof(Food)), CanEditMultipleObjects]
    public class FoodEditor : ItemEditor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            EditorGUILayout.LabelField("Food", EditorStyles.boldLabel);
            Food item = (Food)target;

            item.AddHealth = EditorGUILayout.FloatField("Health", item.AddHealth);
            item.AddWater = EditorGUILayout.FloatField("Water", item.AddWater);
            item.AddFood = EditorGUILayout.FloatField("Food", item.AddFood);

            EditorUtility.SetDirty(item);
        }
    }

    [CustomEditor(typeof(Other)), CanEditMultipleObjects]
    public class OtherEditor : ItemEditor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            Other item = (Other)target;
            item.Type = Item.TypeItem.Other;
        }
    }

    [CustomEditor(typeof(Build)), CanEditMultipleObjects]
    public class BuildEditor : ItemEditor
    {
        protected override void OnEnable()
        {
            base.OnEnable();
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            Build item = (Build)target;
            item.Type = Item.TypeItem.Build;

            EditorGUILayout.LabelField("Build", EditorStyles.boldLabel);

            item.Layer = (Build.Layers)EditorGUILayout.EnumPopup("Layer", item.Layer);
            item.CanFloor = EditorGUILayout.Toggle("Can Floor", item.CanFloor);
            item.MaxLevel = EditorGUILayout.IntField("Max Level", item.MaxLevel);
            item.Width = EditorGUILayout.IntField("Width", item.Width);
            item.Height = EditorGUILayout.IntField("Height", item.Height);

            EditorUtility.SetDirty(item);
        }
    }

}
#endif