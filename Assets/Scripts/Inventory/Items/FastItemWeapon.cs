﻿using System;
using UnityEngine;
using Scripts.Items;

[System.Serializable]
public class FastItemWeapon : FastItem, IStatusItem
{
    public static FastItemWeapon None = new FastItemWeapon(0,0,0,0,0,0);
    [SerializeField] private float status;
    [SerializeField] private int clip;


    public float Status => status;
    public int Clip {get {return clip;} set {clip = value;}}

    public FastItemWeapon (int id, int type, float status, int clip) 
    : base (id, type)
    {
        this.status = status;
        this.clip = clip;
    }

    public FastItemWeapon (int id, int type, int count, float status, int clip) 
    : base (id, type, count)
    {
        this.status = status;
        this.clip = clip;
    }

    public FastItemWeapon (int id, int type, int count,int identificator ,float status, int clip) 
    : base (id, type, count, identificator)
    {
        this.status = status;
        this.clip = clip;
    }

    public override bool Equals(object obj)
    {
        if (obj == null)
            return false;

        if(obj is FastItemWeapon weapon)
        {
            return weapon.id == id && weapon.type == type && weapon.count == count && weapon.status == status && weapon.clip == clip && weapon.indexRnd == indexRnd;
        }

        if(obj is FastItem item)
        {
            return item.ID == id && item.Type == type && item.Count == count && item.IndexRnd == indexRnd;
        }

        return false;

    }

    public override int GetHashCode()
    {
        return Tuple.Create(id, type, count, status, clip).GetHashCode();
    }

    public override FastItem Clone()
    {
        return new FastItemWeapon(id, type, count, status, clip) as FastItem;
    }

    public override string ToString()
    {
        return "ID: " + id + ", " + "Type: " + (Item.TypeItem)type + ", " + "Count: " + count +
        ", " + "Status: " + status + ", " + "Clip: " + clip;
    }
}
