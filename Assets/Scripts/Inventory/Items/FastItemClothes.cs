﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Scripts.Items;

[System.Serializable]
public class FastItemClothes : FastItem, IStatusItem
{
    public static FastItemClothes None = new FastItemClothes(0, 0, 0, 0, 0);
    [SerializeField] private float status;

    public float Status => status;

    public FastItemClothes(int id, int type, float status)
    : base(id, type)
    {
        this.status = status;
    }

    public FastItemClothes(int id, int type, int count, float status)
    : base(id, type, count)
    {
        this.status = status;
    }

    public FastItemClothes(int id, int type, int count, int identificator, float status)
    : base(id, type, count, identificator)
    {
        this.status = status;
    }

    public override bool Equals(object obj)
    {
        if (obj == null)
            return false;

        if (obj is FastItemClothes clothes)
        {
            return clothes.id == id && clothes.type == type && clothes.count == count && clothes.status == status && clothes.indexRnd == indexRnd;
        }

        if (obj is FastItem item)
        {
            return item.ID == id && item.Type == type && item.Count == count && item.IndexRnd == indexRnd;
        }

        return false;

    }

    public override int GetHashCode()
    {
        return Tuple.Create(id, type, count, status).GetHashCode();
    }

    public override FastItem Clone()
    {
        return new FastItemClothes(id, type, count, status) as FastItem;
    }

    public override string ToString()
    {
        return "ID: " + id + ", " + "Type: " + (Item.TypeItem)type + ", " + "Count: " + count +
        ", " + "Status: " + status;
    }
}
