﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts.Items
{
    [CreateAssetMenu(fileName = "New Clothes Data", menuName = "Items/Clothes Data", order = 2)]
    public class Clothes : Item
    {
        public enum TypeClothes { Head, Body, Legs, Backpack, Middlepack }

        [Header("Clothes")]
        [SerializeField] private TypeClothes typeClothes;

        public TypeClothes ClothesType { get { return typeClothes; } set { typeClothes = value; } }

        public override FastItem GetFastItem(int count)
        {
            return new FastItemClothes(GetHashCode(), (int)type, count, 1);
        }
    }
}
