﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Scripts.Utility;
namespace Scripts.Items
{

    [CreateAssetMenu(fileName = "New Weapon Data", menuName = "Items/Weapon Data", order = 0)]
    public class Weapon : Item
    {
        public enum TypeWeapon {Melee, Range}
        public enum TypeMeleeWeapon { None, Tree, Rock, Both }
        [Header("Weapon")]
        [SerializeField] private TypeWeapon typeWeapon;
        [SerializeField] private TypeMeleeWeapon typeMeleeWeapon;

        [SerializeField] private RuntimeAnimatorController animatorController;
        [SerializeField] private Count attackType;
        [SerializeField] private WeaponSetting weaponSetting;

        public RuntimeAnimatorController AnimatorController { get { return animatorController; } set { animatorController = value; } }

        public WeaponSetting Setting { get { return weaponSetting; } set { weaponSetting = value; } }
        public Count AttackType { get { return attackType; } set { attackType = value; } }
        public TypeWeapon TypeWeaponAttack { get { return typeWeapon; } set { typeWeapon = value; } }
        public TypeMeleeWeapon TypeMelee { get { return typeMeleeWeapon; } set { typeMeleeWeapon = value; } }

        public override FastItem GetFastItem(int count)
        {
            return new FastItemWeapon(GetHashCode(), (int)type, count, 1, 0);
        }
    }

    [System.Serializable]
    public class WeaponSetting
    {
        [SerializeField] private float weaponAIMMultiply = 5f;
        [SerializeField] private float damage = 1f;
        [SerializeField] private int clipSize = 30;

        [Header("Shoot")]
        [SerializeField] private Ammo.TypeAmmo typeAmmo;
        [SerializeField] private float speedAnimationShoot = 1f;
        [SerializeField] private float speedBullet = 300f;
        [SerializeField] private int countBulletOnShot = 1;
        [SerializeField] private float cooldownBulletShot = 0.2f;
        [SerializeField] private float scatterOffsetOnShoot = 1f;
        [SerializeField] private float maxScatterOffsetOnShoot = 10f;
        [SerializeField] private bool scopeAIM = false;
        [SerializeField] private float aimFow = 30f;

        public float WeaponAIMMultiply { get { return weaponAIMMultiply; } set { weaponAIMMultiply = value; } }
        public float Damage { get { return damage; } set { damage = value; } }
        public int ClipSize { get { return clipSize; } set { clipSize = value; } }

        public Ammo.TypeAmmo TypeAmmo { get { return typeAmmo; } set { typeAmmo = value; } }
        public float SpeedBullet { get { return speedBullet; } set { speedBullet = value; } }
        public float SpeedAnimationShoot { get { return speedAnimationShoot; } set { speedAnimationShoot = value; } }
        public int CountBulletOnShot { get { return countBulletOnShot; } set { countBulletOnShot = value; } }
        public float CooldownBulletShot { get { return cooldownBulletShot; } set { cooldownBulletShot = value; } }
        public float ScatterOffsetOnShoot { get { return scatterOffsetOnShoot; } set { scatterOffsetOnShoot = value; } }
        public float MaxScatterOffsetOnShoot { get { return maxScatterOffsetOnShoot; } set { maxScatterOffsetOnShoot = value; } }
        public bool ScopeAIM { get { return scopeAIM; } set { scopeAIM = value; } }
        public float AimFow { get { return aimFow; } set { aimFow = value; } }
    }
}
