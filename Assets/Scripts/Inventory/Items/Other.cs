﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts.Items
{
    [CreateAssetMenu(fileName = "New Other Data", menuName = "Items/Other Data", order = 5)]
    public class Other : Item
    {

    }
}
