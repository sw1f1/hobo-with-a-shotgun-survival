﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts.Items
{
     [CreateAssetMenu(fileName = "New Ammo Data", menuName = "Items/Ammo Data", order = 1)]
    public class Ammo : Item
    {
        public enum TypeAmmo {Rilfe, Pistol, Rocket, Shootgun, Arrow}
        [SerializeField] private TypeAmmo typeAmmo;
        [SerializeField] private GameObject bulletPrefab;

        public GameObject Bullet { get { return bulletPrefab; } set { bulletPrefab = value; } }
        public TypeAmmo AmmoType { get { return typeAmmo; } set { typeAmmo = value; } }
    }
}
