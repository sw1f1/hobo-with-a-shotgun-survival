﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Scripts.Utility;
using Scripts.Items;
using Scripts.Managers;
public class GenerateItem : MonoBehaviour
{
    [SerializeField] private Count countItemIsGenerated = new Count(1,2);
    [SerializeField] private List<Item> itemsGenerate = new List<Item>();
    private BaseInventory baseInventory;

    private void Start() 
    {
        baseInventory = GetComponent<BaseInventory>();
        if(baseInventory == null)
            return;

        int count = countItemIsGenerated.GetInt();  
        for(int i = 0; i < count; i++)
        {
            Item item;
            if (itemsGenerate.Count == 0)
                item = ItemManager.GetRandomItem();
            else
                item = itemsGenerate[Random.Range(0, itemsGenerate.Count)];

            baseInventory.AddItem(item.GetFastItem());
        }
    }
}
