﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace Scripts.NPS
{
    public class NavMeshAgent : MonoBehaviour
    {
        [SerializeField] private bool showPath = true;

        private NavMeshPath path;

        public void Start()
        {
            path = new NavMeshPath();
        }

        public void ResetPath()
        {
            path.ClearCorners();
        }

        public Vector3[] CalculatePath(Vector3 target)
        {
            NavMesh.CalculatePath(transform.position, target, NavMesh.AllAreas, path);
            return path.corners;
        }

        public void OnDrawGizmos()
        {
            if (!showPath) return;

            if (path == null || path.corners == null) return;

            Gizmos.color = Color.red;
            for (int i = 0; i < path.corners.Length; i++)
            {
                Gizmos.DrawSphere(path.corners[i], 0.1f);
                if (i != path.corners.Length - 1)
                {
                    Gizmos.DrawLine(path.corners[i], path.corners[i + 1]);
                }
            }
        }
    }

}
