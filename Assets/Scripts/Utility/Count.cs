﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts.Utility
{
    [System.Serializable]
    public class Count
    {
        public float min = 5f;
        public float max = 10f;
        public bool infinity = false;

        public Count(float min, float max, bool infinity = false)
        {
            this.min = min;
            this.max = max;
            this.infinity = infinity;
        }

        public float GetCount()
        {
            return Random.Range(min, max);
        }
        public int GetInt()
        {
            if (min == max) return (int)max;
            return Random.Range((int)min, (int)max);
        }
    }
}
