using UnityEngine;

public class Vector
{
	public static Vector3 Rotate(Vector3 point, float angle)
	{	
		return Quaternion.Euler(Random.Range(-angle, angle), Random.Range(-angle, angle), Random.Range(-angle, angle)) * point;
	}

    public static float GetAngle(Vector3 _vector1, Vector3 _vector2, Vector3 _planeNormal)
    {
        float _angle = Vector3.Angle(_vector1, _vector2);
        float _sign = Mathf.Sign(Vector3.Dot(_planeNormal, Vector3.Cross(_vector1, _vector2)));

        float _signedAngle = _angle * _sign;

        return _signedAngle;
    }
}
