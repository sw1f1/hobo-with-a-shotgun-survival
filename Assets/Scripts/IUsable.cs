﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IUsable
{
    bool IsActive { get; }
    float TimeUsed { get; }
    Transform transform {get;}
    string TextUse { get; }
    bool CombineWithOther { get; set; }

    void OnClickUse();
    void OnUse();
    void Show(bool value);

    Action<IUsable> OnUsed { get; set; }
}
