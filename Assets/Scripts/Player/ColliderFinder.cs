﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Scripts.Managers;
using Scripts.MovementController;
using Scripts.MovementController.States;

public class ColliderFinder : MonoBehaviour
{
    private AgentController agentController;
    private List<IUsable> objects = new List<IUsable>();

    private void Awake()
    {
        agentController = GetComponentInParent<AgentController>();
    }

    private void OnEnable() 
    {
        InputManager.OnClickUse += Use;
    }

    private void OnDisable() 
    {
         InputManager.OnClickUse -= Use;
    }

    private void Update()
    {
        if (agentController.TypeState == AgentController.StateType.UsedObject)
            return;

        IUsable obj = objects.OrderBy(x => (x.transform.position - transform.position).magnitude).Where(x => x.IsActive).FirstOrDefault();

        if (obj != null)
            EventManager.OnVisibleUseObject?.Invoke(true, obj);
    }

    private void OnTriggerEnter(Collider other) 
    {
        IUsable usable = other.GetComponent<IUsable>();

        if(usable != null)
        {
            usable.Show(true);
            objects.Add(usable);

            IUsable obj = objects.OrderBy(x => (x.transform.position - transform.position).magnitude).Where(x=> x.IsActive).FirstOrDefault();

            if(obj != null)
                EventManager.OnVisibleUseObject?.Invoke(true, obj);
        }
    }

    private void OnTriggerExit(Collider other) 
    {
        IUsable usable = other.GetComponent<IUsable>();
        if(usable != null)
        {
            OnTriggerExit(usable);
        }
    }

    public void OnTriggerExit(IUsable usable)
    {
        usable.Show(false);
        objects.Remove(usable);

        if (objects.Count == 0)
        {
            EventManager.OnVisibleUseObject?.Invoke(false, null);
        }
    }

    public void Use()
    {
        if (agentController.TypeState == AgentController.StateType.UsedObject)
            return;

        if(objects.Count == 0)
            return;

        IUsable obj = objects.OrderBy(x=> (x.transform.position - transform.position).magnitude).Where(x => x.IsActive).FirstOrDefault();

        if (obj == null)
            return;

        var usedState = agentController.GetState(AgentController.StateType.UsedObject);
        (usedState as UsedObjectState).SetUsableObject(obj);
        agentController.ChangeState(AgentController.StateType.UsedObject);

        obj.OnClickUse();
        obj.OnUsed += OnTriggerExit;

        EventManager.OnUsedObject?.Invoke(obj);
    }


}
