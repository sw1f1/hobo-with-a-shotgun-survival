﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Scripts.Items;
using Scripts.Managers;
using Scripts.World.GridSystem;

public class BuildContainer : WeaponContainer
{
    [SerializeField] private LayerMask mask;
    [SerializeField] private Build.Direction direction = Build.Direction.Down;

    private BuildObject buildObject;

    private FastItem buildFastItem;
    private Build build;

    private ItemContainer itemContainer;

    private Vector3 position;
    List<Vector2Int> positions;
    private Quaternion rotation;
    private bool canBuild;
    private int level = 0;
    
    private void Awake()
    {
        itemContainer = GetComponent<ItemContainer>();
    }

    private void OnEnable()
    {
        EventManager.OnSelectBuildItem += OnSelectBuildItem;
    }


    private void OnDestroy()
    {
        EventManager.OnSelectBuildItem -= OnSelectBuildItem;

        if(buildObject != null)
            Destroy(buildObject.gameObject);
    }

    public override void InitWeapon(WeaponController weaponController)
    {
        base.InitWeapon(weaponController);
    }

    private void OnSelectBuildItem(FastItem item)
    {
        CreateNewObjectBuild(item);
    }

    private void CreateNewObjectBuild(FastItem item)
    {
        if(buildObject != null)
            Destroy(buildObject.gameObject);

        buildFastItem = item;
        build = (Build)ItemManager.GetItem(buildFastItem);
        RaycastHit hit;
        buildObject = Instantiate(build.Go, GridManager.Grid.WorldToGridCenterPosition(GetPointPosition(out hit)), rotation).GetComponent<BuildObject>();
        buildObject.OnBuild();
    }

    public override void OnStartAttack()
    {
        base.OnStartAttack();

        if (buildObject == null)
            return;

        if (canBuild)
        {
            buildObject.transform.position = position;
            buildObject.transform.rotation = rotation;

            foreach (var pos in positions)
            {
                GridManager.Grid.GetGridObject(pos).AddGameObject(buildObject.gameObject, build.Layer, direction, level);
            }
            buildObject.OnBuilded();
            buildObject = null;
            CreateNewObjectBuild(buildFastItem);
        }
    }

    private void Update()
    {
        if(!isAttack && buildObject != null)
        {
            positions = GetBuildingObjectAllPosition();
            canBuild = CheckBuild(positions);

            UpdateBuild();
            UpdateRotation();
        }
    }

    public override void OnEndAttack()
    {
        base.OnEndAttack();
    }

    private void UpdateBuild()
    {
        Ray ray = weaponController.GetLookRay();
        RaycastHit hit;
        Vector3 hitPoint = GetPointPosition(out hit) - new Vector3(ray.direction.x, 0, ray.direction.z);
        position = GridManager.Grid.WorldToGridCenterPosition(hitPoint);

        if (hit.transform != null && hit.transform.gameObject.layer == 14 )
        {
            level = Mathf.Clamp(Mathf.FloorToInt(hitPoint.y / 2f), 0, build.MaxLevel);
        }
        else
            level = 0;

        position += new Vector3(0, level * 3f, 0);
        buildObject.transform.position = Vector3.Lerp(buildObject.transform.position, position, Time.deltaTime * 20f);
    }

    private void UpdateRotation()
    {
        buildObject.transform.rotation = Quaternion.Slerp(buildObject.transform.rotation, rotation, Time.deltaTime * 20f);
    }

    private Vector3 GetPointPosition(out RaycastHit hit)
    {
        Ray ray = weaponController.GetLookRay();
        if (Physics.Raycast(ray, out hit, 10, mask))
        {
            return hit.point;
        }else
        {
            Vector3 point = ray.origin + ray.direction * 10;
            if (Physics.Raycast(point, Vector3.down, out hit, 100, mask))
            {
                return hit.point;
            }
        }

        return position;
    }

    private bool CheckBuild(List<Vector2Int> positions)
    {
        bool isFree = true;
        bool isFloor = true;
        foreach(var position in positions)
        {
            GridObject gridObject = GridManager.Grid.GetGridObject(position);

            if(!build.CanFloor && level > 0 && gridObject.IsFree(Build.Layers.Floor, direction, level))
            {
                isFree = false;
                break;
            }

            if(build.CanFloor && build.Layer != Build.Layers.Wall)
            {
                isFloor = gridObject.IsFloor(level);
                if (!isFloor)
                    break;
            }

            if(build.CanFloor && build.Layer == Build.Layers.Wall)
            {
                isFloor = gridObject.IsWallLevelDown(level, direction);
                if (!isFloor)
                    break;
            }

            if (!gridObject.IsFree(build.Layer, direction, level))
            {
                isFree = false;
                break;
            }
        }

        bool box = buildObject.CheckBox(mask, direction);
        bool canBuild = !box && isFree && isFloor;
        buildObject.VisualCheckBuild(canBuild);

        return canBuild;
    }


    private List<Vector2Int> GetBuildingObjectAllPosition()
    {
        Vector2Int offset = GridManager.Grid.GetGridObjectPosition(buildObject.transform.position);
        List<Vector2Int> position = build.GetGridPositions(offset, direction);
        return position;
    }

    public override void OnReload()
    {
        direction = Build.GetNextDirection(new Vector2(0, 1), direction);
        rotation = Build.GetRotationAngle(direction);
    }
}
