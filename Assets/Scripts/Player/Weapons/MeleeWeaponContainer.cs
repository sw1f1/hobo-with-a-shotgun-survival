﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Scripts.Utility;
using Scripts.Items;
using Scripts.Managers;

public class MeleeWeaponContainer : WeaponContainer
{
    private FastItemWeapon currentWeapon;
    private Weapon weaponItem;
    private Count attackType = new Count(0,0);

    private Collider collider;

    public override void InitWeapon(WeaponController weaponController)
    {
        base.InitWeapon(weaponController);
        
        if(Item == null || Item.FItem == null)
            return;

        collider = GetComponent<Collider>();

        SetTriggerOnCollider(true);

        currentWeapon = Item.FItem as FastItemWeapon;
        weaponItem = ItemManager.GetItem(currentWeapon) as Weapon;

        if (weaponItem != null)
        {
            attackType = weaponItem.AttackType;
            weaponController.WalkerController.Animation.SetRuntimeAnimatorController(weaponItem.AnimatorController);
        }

        weaponController.OnUpdateClip?.Invoke(currentWeapon, null);
    }

    public override void OnStartAttack()
    {
        base.OnStartAttack();
        UpdateAttackType();
        weaponController.WalkerController.Animation.SetBool("IsMeleeAttack", true);

        SetTriggerOnCollider(false);
    }

    public override void OnEndAttack()
    {
        base.OnEndAttack();
        weaponController.WalkerController.Animation.SetBool("IsMeleeAttack", false);

        SetTriggerOnCollider(true);
    }

    private void SetTriggerOnCollider(bool value)
    {
        if (collider != null)
            collider.isTrigger = value;
    }

    public override void UpdateAttackType()
    {
        if (!isAttack)
        {
            weaponController.WalkerController.ChangeStateOnLast();
            return;
        }

        weaponController.WalkerController.ChangeState(Scripts.MovementController.AgentController.StateType.Idle);
        weaponController.WalkerController.Animation.SetFloat("AttackType", attackType.GetInt());
    }

    private void OnCollisionEnter(Collision collision)
    {
        IHealth target = collision.transform.GetComponent<IHealth>();

        if(target != null)
            target.OnDamage(weaponItem.Setting.Damage, currentWeapon, weaponController.HealthController);
    }

    private void OnTriggerEnter(Collider other)
    {
        IHealth target = other.transform.GetComponent<IHealth>();

        if (target != null)
            target.OnDamage(weaponItem.Setting.Damage, currentWeapon, weaponController.HealthController);
    }
}
