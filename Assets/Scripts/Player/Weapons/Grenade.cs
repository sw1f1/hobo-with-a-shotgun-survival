﻿using Scripts.Items;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grenade : MonoBehaviour
{
    [SerializeField] private GameObject explode;
    private float damage;
    private ThrowContainer throwContainer;
    private ItemContainer itemContainer;

    private void Start()
    {
        throwContainer = GetComponent<ThrowContainer>();
        itemContainer = GetComponent<ItemContainer>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        ContactPoint contact = collision.GetContact(0);
        Instantiate(explode, contact.point, Quaternion.identity);

        //Physics.SphereCastAll(contact.point, )

        IHealth target = collision.transform.GetComponent<IHealth>();
        if (target != null && target != throwContainer.Owner)
            target.OnDamage(damage, itemContainer.FItem, throwContainer.Owner);

        Destroy(gameObject);
    }

}
