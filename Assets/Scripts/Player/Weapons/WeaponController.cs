﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Scripts.Items;
using Scripts.Managers;
using Scripts.MovementController;

public class WeaponController : MonoBehaviour
{
    public Action<FastItemWeapon, FastItem> OnUpdateClip;
    [SerializeField] private Transform leftArmContainer;
    [SerializeField] private Transform rigthArmContainer;

    [SerializeField] private Transform aimPivot;
    [SerializeField] private BallisticTrajectoryRenderer trajectoryRenderer;

    [Header("Defolt")]
    [SerializeField] private Transform defoultAIMIKWeapon;
    [SerializeField] private RuntimeAnimatorController defoultAnimController;
    [SerializeField] private WeaponContainer defoultWeapon;

    private BaseInventory inventory;
    private AgentController walkerController;
    private HealthController healthController;

    private FastItem item;

    private WeaponContainer weaponGO;
    private WeaponIK weaponIK;

    private bool isAttack;

    public BaseInventory Inventory => inventory;
    public AgentController WalkerController => walkerController;
    public HealthController HealthController => healthController;
    public WeaponIK IK => weaponIK;

    private void Awake () 
    {
        weaponIK = GetComponent<WeaponIK>();
        walkerController = GetComponent<AgentController>();
        healthController = GetComponent<HealthController>();
    }

    private void Start()
    {
        defoultWeapon.InitWeapon(this);
        weaponGO = defoultWeapon;
    }

    public void SetWeapon(FastItem fitem, BaseInventory inventory)
    {
        if (item == fitem)
            return;

        if (item != null && item.Equals(fitem))
            return;

        this.inventory = inventory;
        this.item = fitem;

        weaponIK.SetAim(null);

        if (fitem == null || fitem.Equals(FastItem.None))
        {
            SetDefoultWeapon();
        }

        walkerController.Animation.SetTrigger("Remove Weapon");
    }

    public void SwitchWeapon()
    {
        if (weaponGO != null && weaponGO != defoultWeapon)
            Destroy(weaponGO.gameObject);
        
        if (item == null || item.Equals(FastItem.None))
        {
            weaponGO = defoultWeapon;
            walkerController.Animation.SetRuntimeAnimatorController(defoultAnimController);
        }
        else
        {
            Item currentItem = ItemManager.GetItem(item);
            weaponGO = Instantiate(currentItem.Go, rigthArmContainer).GetComponent<WeaponContainer>();

            FastItem itemClone = item.Clone();
            itemClone.Count = 1;
            weaponGO.GetComponent<ItemContainer>().Init(itemClone);
            weaponGO.InitWeapon(this);
        }

        walkerController.Animation.SetTrigger("Take Weapon");
    }

    public void TakedWeapon()
    {
        weaponIK.SetAim(weaponGO.AimPivot);
    }


    public Vector3 GetTargetPosition(Transform aim)
    {
        return weaponIK.GetTargetPosition(aim);
    }

    public Vector3 GetLookDirection()
    {
        return Camera.main.transform.forward;
    }

    public Ray GetLookRay()
    {
        return new Ray(Camera.main.transform.position, GetLookDirection());
    }

    public void SetDefoultWeapon()
    {
        this.item = null;
        walkerController.Animation.SetFloat("SpeedShoot", 1f);

        OnUpdateClip?.Invoke(null, null);
    }

    public void OnAttack()
    {
        isAttack = true;
        weaponGO.OnStartAttack();
    }

    public void OnEndAttck()
    {
        isAttack = false;
        weaponGO.OnEndAttack();
    }

    public void NextAttack()
    {
        weaponGO.UpdateAttackType();
    }

    public void OnReload()
    {
        weaponGO.OnReload();
    }

    public void Reloaded()
    {
        weaponGO.Reloaded();
    }

    public float GetScatter()
    {
        return weaponGO.GetScatter();
    }

    public float GetFow()
    {
        return (weaponGO as RangeWeaponContainer).GetFow();
    }
    
    public bool CanAim()
    {
        return (weaponGO != null && (weaponGO is RangeWeaponContainer));
    }

    public bool CanAimScope()
    {
        return (ItemManager.GetItem(item) as Weapon).Setting.ScopeAIM;
    }

    public void OnThrow()
    {
        if(weaponGO != null && weaponGO is ThrowContainer)
        {
            weaponGO.OnThrow();
            weaponGO = defoultWeapon;
            item.Count -= 1;
            inventory.UpdateState();
        }
    }

    public void UpdateBallisticRender()
    {
        Vector3 offset = transform.right * 2f;
        trajectoryRenderer.SetBallisticValue(weaponGO.transform.position, (weaponGO as ThrowContainer).ForceThrow);
    }

    public void ClearBallisticRender()
    {
        trajectoryRenderer.ClearTrajectory();
    }
}
