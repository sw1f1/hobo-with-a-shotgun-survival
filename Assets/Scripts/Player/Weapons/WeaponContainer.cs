﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Scripts.Items;

[RequireComponent(typeof(ItemContainer))]
public class WeaponContainer : MonoBehaviour
{
    [SerializeField] private Transform aimPivot;
    [SerializeField] private bool drawGizmozDebug = false;

    protected bool isAttack;
    protected float scatterOffsetOnShoot;
    protected WeaponController weaponController;

    private ItemContainer container;

    public ItemContainer Item => container;
    public Transform AimPivot => aimPivot;

    private void Awake()
    {
        container = GetComponent<ItemContainer>();
    }

    public virtual void InitWeapon(WeaponController weaponController)
    {
        this.weaponController = weaponController;
    }

    public virtual void OnStartAttack()
    {
        isAttack = true;
    }

    public virtual void UpdateAttackType()
    {

    }

    public virtual void OnReload()
    {

    }

    public virtual void Reloaded()
    {

    }

    public virtual void OnEndAttack()
    {
        isAttack = false;
    }

    public virtual void OnThrow()
    {

    }

    public virtual float GetScatter()
    {
        return InputManager.GetScatterCount() + scatterOffsetOnShoot;
    }

    public Vector3 CalculateVectorScatter(Vector3 direction, float scatter)
    {
        return Vector.Rotate(direction, Random.Range(0f - scatter, scatter));
    }

    private void OnDrawGizmos()
    {
        if (!drawGizmozDebug)
            return;

        Gizmos.color = Color.red;
        Gizmos.DrawLine(aimPivot.position, aimPivot.position + aimPivot.forward);
        Gizmos.DrawSphere(aimPivot.position + aimPivot.forward, 0.01f);
    }
}
