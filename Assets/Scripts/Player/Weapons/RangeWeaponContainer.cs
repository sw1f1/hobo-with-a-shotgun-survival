﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Scripts.Items;
using Scripts.Managers;

public class RangeWeaponContainer : WeaponContainer
{
    [SerializeField] private ParticleSystem flesh;

    private FastItemWeapon currentWeapon;
    private Weapon weaponItem;

    private FastItem currentAmmo;
    private Ammo ammoItem;

    private bool isReload;

    public override void InitWeapon(WeaponController weaponController)
    {
        base.InitWeapon(weaponController);

        currentWeapon = Item.FItem as FastItemWeapon;
        weaponItem = ItemManager.GetItem(currentWeapon) as Weapon;

        ammoItem = ItemManager.GetAmmo(weaponItem.Setting.TypeAmmo);
        currentAmmo = weaponController.Inventory.FindAmmo(ammoItem);

        weaponController.WalkerController.Animation.SetRuntimeAnimatorController(weaponItem.AnimatorController);

        weaponController.WalkerController.Animation.SetFloat("SpeedShoot", weaponItem.Setting.SpeedAnimationShoot);
        weaponController.WalkerController.Animation.SetFloat("AttackType", weaponItem.AttackType.GetInt());

        weaponController.OnUpdateClip?.Invoke(currentWeapon, currentAmmo);

        scatterOffsetOnShoot = weaponItem.Setting.ScatterOffsetOnShoot;
    }

    public override void OnStartAttack()
    {
        base.OnStartAttack();
        StartCoroutine(Shoot());
    }

    public float GetFow()
    {
        return weaponItem.Setting.AimFow;
    }

    private IEnumerator Shoot()
    {
        while (isAttack)
        {
            if (!isReload && currentWeapon.Clip <= 0)
            {
                currentWeapon.Clip = 0;
                weaponController.OnUpdateClip?.Invoke(currentWeapon, currentAmmo);
                OnReload();
                yield return null;
            }

            if (!isReload && currentWeapon.Clip > 0)
            {
                currentWeapon.Clip -= 1;
                weaponController.OnUpdateClip?.Invoke(currentWeapon, currentAmmo);

                weaponController.WalkerController.Animation.SetTrigger("IsAttack");
                ShootBullet();

                yield return new WaitForSeconds(weaponItem.Setting.CooldownBulletShot);
                scatterOffsetOnShoot += weaponItem.Setting.ScatterOffsetOnShoot;
                scatterOffsetOnShoot = Mathf.Clamp(scatterOffsetOnShoot, 0, weaponItem.Setting.MaxScatterOffsetOnShoot);
            }
            else
            {
                yield return null;
            }
        }

        scatterOffsetOnShoot = weaponItem.Setting.ScatterOffsetOnShoot;
    }

    private void ShootBullet()
    {
        SetActiveFlesh();
        for (int i = 0; i < weaponItem.Setting.CountBulletOnShot; i++)
        {
            Bullet bullet = Instantiate(ammoItem.Bullet, AimPivot.position, Quaternion.identity).GetComponent<Bullet>();

            float scatter = GetScatter() / 10f;

            Vector3 direction = CalculateVectorScatter(AimPivot.forward, scatter);
            bullet.Shoot(direction * weaponItem.Setting.SpeedBullet, weaponItem.Setting.Damage, currentWeapon, weaponController.HealthController);
        }
    }

    public override void OnReload()
    {
        if (isReload || currentWeapon.Clip == weaponItem.Setting.ClipSize)
            return;

        if (currentAmmo.Count <= 0)
        {
            isAttack = false;
        }
        else
        {
            isReload = true;
            weaponController.WalkerController.Animation.SetTrigger("IsReload");
        }
    }

    public override void Reloaded()
    {
        isReload = false;

        int clip = currentWeapon.Clip;
        clip += (currentAmmo.Count > weaponItem.Setting.ClipSize - clip) ? weaponItem.Setting.ClipSize - clip : currentAmmo.Count;
        currentAmmo.Count -= clip - currentWeapon.Clip;
        currentWeapon.Clip = clip;

        if (currentAmmo.Count == 0)
        {
            weaponController.Inventory.DeleteItem(currentAmmo);
            currentAmmo = weaponController.Inventory.FindAmmo(ammoItem);
        }

        weaponController.OnUpdateClip?.Invoke(currentWeapon, currentAmmo);
    }

    public override void OnEndAttack()
    {
        base.OnEndAttack();
    }

    public void SetActiveFlesh()
    {
        if (flesh == null)
            return;

        flesh.Play();
    }

    public override float GetScatter()
    {
        return (InputManager.GetScatterCount() + scatterOffsetOnShoot) * weaponItem.Setting.WeaponAIMMultiply;
    }
}
