﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Scripts.Utility;
using Scripts.Items;
using Scripts.Managers;

public class ThrowContainer : WeaponContainer
{
    [SerializeField] private Vector3 forceThrow = new Vector3(0, 4f, 5f);

    private FastItem currentItem;

    public Vector3 ForceThrow => Vector3.up * forceThrow.y + weaponController.GetLookDirection() * forceThrow.z;

    private ItemContainer itemContainer;
    private IHealth owner;

    public IHealth Owner => owner;

    private void Awake()
    {
        itemContainer = GetComponent<ItemContainer>();
        itemContainer.Take();
        transform.localPosition = Vector3.zero;
    }

    public override void InitWeapon(WeaponController weaponController)
    {
        base.InitWeapon(weaponController);

        if (Item == null || Item.FItem == null)
            return;

        owner = weaponController.HealthController;

        currentItem = Item.FItem;
    }

    public override void OnStartAttack()
    {
        base.OnStartAttack();
        weaponController.WalkerController.Animation.SetTrigger("ReadyThrow");
        UpdateAttackType();
    }

    private void Update()
    {
        if(isAttack)
            weaponController.UpdateBallisticRender();
    }

    public override void OnEndAttack()
    {
        base.OnEndAttack();
        weaponController.WalkerController.Animation.SetTrigger("Throw");
    }

    public override void OnThrow()
    {
        gameObject.transform.parent = null;

        itemContainer.Drop(ForceThrow);
        weaponController.ClearBallisticRender();
    }

    public override void UpdateAttackType()
    {
       
    }
}
