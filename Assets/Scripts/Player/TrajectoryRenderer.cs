﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class TrajectoryRenderer : MonoBehaviour
{
    [SerializeField] private Transform aim;
    [SerializeField] private float velocity;
    [SerializeField] private float angle;
    [Range(1, 100)]
    [SerializeField] private int resolution = 10;

    private float gravity;
    private float radianAngle;
    private LineRenderer lineRenderer;

    private void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();
        gravity = Mathf.Abs(Physics.gravity.y);
    }

    private void Start()
    {
        RenderTrajectory();
    }

    private void Update()
    {
        RenderTrajectory();
    }

    private void RenderTrajectory()
    {
        lineRenderer.positionCount = resolution + 2;
        lineRenderer.SetPositions(CalculateLineRenderArray());
    }

    private Vector3[] CalculateLineRenderArray()
    {
        Vector3[] array = new Vector3[resolution + 2];

        //float maxDistance = (velocity * velocity * Mathf.Sin(2 * radianAngle)) / gravity;

        float maxDistance = (new Vector3(aim.position.x, 0, aim.position.z) - transform.position).magnitude;
        maxDistance += Mathf.Abs(aim.position.y) - Mathf.Abs(transform.position.y);
        radianAngle = GetRadianAngle(maxDistance);
        GetAngle();
        velocity = GetVelocity(maxDistance);

        for (int i = 0; i <= resolution; i++)
        {
            float t = (float)i / (float)resolution;
            array[i] = transform.position + CalculatePoint(t, maxDistance);
        }

        array[resolution+1] = aim.position;

        return array;
    }

    public float GetVelocity(float maxDistance)
    {
        return Mathf.Sqrt((maxDistance * gravity) / Mathf.Sin(2 * radianAngle));
    }

    private Vector3 CalculatePoint(float t, float maxDistance)
    {
        float x = t * maxDistance;
        float y = x * Mathf.Tan(radianAngle) - ((gravity * x * x) / (2 * velocity * velocity * Mathf.Cos(radianAngle) * Mathf.Cos(radianAngle)));

        Vector3 direction = aim.position - transform.position;
        direction.y = 0;
        float angleX = Vector3.Angle(transform.forward, direction);
        if (direction.x < 0)
            angleX *= -1;

        return Quaternion.Euler(0, angleX, 0) * new Vector3(0, y, x);
    }

    private float GetRadianAngle(float maxDistance)
    {
        Vector3 direction = (aim.position - transform.position).normalized;
        direction.x = 0;
        float angleY = Vector3.Angle(transform.forward, direction);
        if(angleY < 5f)
        {
            angleY = 45f;
        }
        //Debug.Log(angleY);
        return Mathf.Deg2Rad * (angle);
    }

    private float GetAngle()
    {
        Vector3 direction = (aim.position - transform.position).normalized;
        Vector3 up = aim.up;
        float angleY = Vector3.Angle(up, direction);
        Debug.Log(angleY);
        return Mathf.Deg2Rad * (angleY);
    }
}
