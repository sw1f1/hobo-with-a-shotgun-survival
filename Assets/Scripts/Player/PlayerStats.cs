﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Scripts.Managers
{
    public class PlayerStats : Singleton<PlayerStats>
    {
        [SerializeField] private GameObject playerGO;

        private PlayerInventory inventory;
        private PlayerHealthController healthController;

        public static GameObject PlayerGO => Instance.playerGO;
        public static PlayerInventory Inventory => Instance.inventory;

        private void Start() 
        {
            inventory = GetComponent<PlayerInventory>();
            InitPlayer();
        }

        private void InitPlayer()
        {
            healthController = playerGO.GetComponent<PlayerHealthController>();
            healthController.Health.SetValues(100, 100);
            healthController.Food.SetValues(100, 100);
            healthController.Water.SetValues(100, 100);
        }

        public static void SetPlayerGO(GameObject player)
        {
            Instance.playerGO = player;
            Instance.InitPlayer();
        }
    }
}
