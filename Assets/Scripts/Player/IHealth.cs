﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IHealth
{
    ValueStats Health { get; }
    Transform Transform { get; }

    void OnDamage(float damage, FastItem currentWeapon, IHealth target);
}
