﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private GameObject hole;

    private float damage;
    private FastItemWeapon currentWeapon;
    private IHealth owner;

    private Rigidbody rigidbody;

    private void Awake() {
        rigidbody = GetComponent<Rigidbody>();
    }

    public void Shoot(Vector3 direction, float damage, FastItemWeapon currentWeapon, IHealth owner)
    {
        this.damage = damage;
        this.currentWeapon = currentWeapon;
        this.owner = owner;
        transform.rotation = Quaternion.LookRotation(direction);

        rigidbody.AddForce(direction, ForceMode.Impulse);
        StartCoroutine(DestoryBullet());
    }

    IEnumerator DestoryBullet()
    {
        yield return new WaitForSeconds(5f);
        Destroy(gameObject);
    }

    private void OnCollisionEnter(Collision other) 
    {
        StopAllCoroutines();
        ContactPoint contact = other.GetContact(0);
        GameObject h =  Instantiate(hole, contact.point, Quaternion.FromToRotation(Vector3.forward, contact.normal));
        h.transform.SetParent(other.transform);

        IHealth target = other.transform.GetComponent<IHealth>();

        if (target != null)
            target.OnDamage(damage, currentWeapon, owner);

        Destroy(gameObject);

    }
}
