﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponIK : MonoBehaviour
{
    [SerializeField] private Transform target;
    [SerializeField] private HumanBone[] humanBones;
    [SerializeField] private int iterations = 10;
    [Range(0, 1)]
    [SerializeField] private float weight = 1f;
    [SerializeField] private float angleLimit = 90f;
    [SerializeField] private float distanceLimit = 1.5f;
    [SerializeField] private float smooth = 10f;
    private Transform aim;
    private Transform[] bonesTrahsforms;
    private Vector3 lastDirection;
    private Vector3 targetPosition;
    private bool eneable = true;

    private void Start() {
        Animator animator = GetComponent<Animator>();
        bonesTrahsforms = new Transform[humanBones.Length];
        for(int i = 0; i< bonesTrahsforms.Length; i++)
        {
            bonesTrahsforms[i] = animator.GetBoneTransform(humanBones[i].bone);
        }
    }

    private void LateUpdate() 
    {
        if (!eneable) return;

        if(target == null || aim == null)
            return;

        targetPosition = Vector3.Lerp(targetPosition, GetTargetPosition(aim), Time.deltaTime * smooth);

        for(int i = 0; i< iterations; i++)
        {
            for(int b = 0; b < bonesTrahsforms.Length; b++)
            {
                Transform bone = bonesTrahsforms[b];
                float boneWeight = humanBones[b].weight * weight;
                AimAtTarget(bone, targetPosition, boneWeight);
            }
        }
    }

    public Vector3 GetTargetPosition(Transform aim)
    {
        Vector3 targetDirection = target.position - aim.position;
        Vector3 aimDirection = aim.forward;
        float blendOut = 0f;

        float targetAngle = Vector3.Angle(targetDirection, aimDirection);
        if(targetAngle > angleLimit)
        {
            blendOut += (targetAngle - angleLimit) / 50f;    
        }

        float targetDistance = targetDirection.magnitude;
        if(targetDistance < distanceLimit)
        {
            blendOut += distanceLimit - targetDistance;  
        }

        Vector3 direction = Vector3.Slerp(targetDirection, lastDirection, blendOut);
        lastDirection = direction;
        return aim.position + direction;
    }


    private void AimAtTarget(Transform bone, Vector3 position, float weight)
    {
        if(bone == null)
            return;

        Vector3 aimDirection = aim.forward;
        Vector3 targetDirection = position - aim.position;
        Quaternion aimTowards = Quaternion.FromToRotation(aimDirection, targetDirection);
        Quaternion blendedRotation = Quaternion.Slerp(Quaternion.identity, aimTowards, weight);
        bone.rotation = blendedRotation * bone.rotation;
    }

    public void SetAim(Transform aim)
    {
        this.aim = aim;
        targetPosition = target.position;
    }

    public void SetActive(bool value)
    {
        eneable = value;
    }
}

[System.Serializable]
public class HumanBone
{
    public HumanBodyBones bone;
    public float weight = 1f;
}
