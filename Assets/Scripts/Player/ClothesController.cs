﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Scripts.Items;
using Scripts.Managers;

public class ClothesController : MonoBehaviour
{
    [SerializeField] private Transform rootBone;
    [SerializeField] private Transform model;

    [SerializeField] private SkinnedMeshRenderer[] originalsMeshRenderer;
    [SerializeField] private GameObject meshPrefab;

    [SerializeField] private SkinnedMeshRenderer[] defultMeshRenderer;
    [SerializeField] private GameObject[] clothesModel;

    [ContextMenu("FindSkinnedMeshRenderer")]
    public void FindSkinnedMeshRenderer()
    {
        originalsMeshRenderer = meshPrefab.GetComponentsInChildren<SkinnedMeshRenderer>();
    }

    public void SetClothes(FastItemClothes fastItem, Clothes.TypeClothes type)
    {
        if (fastItem == null || fastItem.Equals(FastItemClothes.None))
        {
            SetDefultClothes(type);
            return;
        }

        if (clothesModel[(int)type] != null)
        {
            clothesModel[(int)type].SetActive(false);
        }


        Clothes clothes = ItemManager.GetItem(fastItem) as Clothes;
        SkinnedMeshRenderer go = FindClothes(clothes.Go.GetComponent<SkinnedMeshRenderer>());
        clothesModel[(int)type] = go.gameObject;
        go.gameObject.SetActive(true);

        /*
        GameObject go = Instantiate(clothes.Go, model);
        clothesModel[(int)type] = go;
        CalculateSkinningMeshBone(go.GetComponent<SkinnedMeshRenderer>(), clothes);
        */
    }

    private void SetDefultClothes(Clothes.TypeClothes type)
    {
        if (clothesModel[(int)type] != null)
        {
            clothesModel[(int)type].SetActive(false);
        }

        if((int)type < 3)
            defultMeshRenderer[(int)type].gameObject.SetActive(true);
    }

    private void CalculateSkinningMeshBone(SkinnedMeshRenderer current, Clothes clothes)
    {
        SkinnedMeshRenderer original = originalsMeshRenderer.Where(x => x.sharedMesh.Equals(current.sharedMesh)).FirstOrDefault();
        current.rootBone = original.rootBone;
        current.bones = original.bones;
    }

    private SkinnedMeshRenderer FindClothes(SkinnedMeshRenderer current)
    {
        return originalsMeshRenderer.Where(x => x.sharedMesh.Equals(current.sharedMesh)).FirstOrDefault();
    }
}
