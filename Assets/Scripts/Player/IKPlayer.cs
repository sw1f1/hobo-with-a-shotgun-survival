﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CMF;
public class IKPlayer : MonoBehaviour
{
    [SerializeField] private float lookIKWeight;
    [SerializeField] private float eyesWeight;
    [SerializeField] private float headWeight;
    [SerializeField] private float bodyWeight;
    [SerializeField] private float clampWeight;
    [SerializeField] private Transform targetAIM;
    [SerializeField] private Vector3 offset;
    private Animator animator;
    private Vector3 smooth;

    private void Awake () 
    {
		animator = GetComponentInChildren<Animator>();
        smooth = targetAIM.position;
	}

    private void OnAnimatorIK(int layerIndex) 
    {
        if(layerIndex > 0) return;

        animator.SetLookAtWeight(lookIKWeight, bodyWeight, headWeight, eyesWeight, clampWeight);

        float dist = (transform.position - targetAIM.position).magnitude;

        smooth = Vector3.Lerp(smooth, targetAIM.position + targetAIM.right * offset.x * dist, Time.deltaTime * 10f);

        animator.SetLookAtPosition(smooth);

    }
    
}
