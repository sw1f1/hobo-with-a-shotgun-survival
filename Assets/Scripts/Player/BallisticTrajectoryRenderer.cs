﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class BallisticTrajectoryRenderer : MonoBehaviour
{
    [SerializeField] private Vector3 startPosition;
    [SerializeField] private Vector3 velocity;
    [SerializeField] private float trajectoryVertDist = 0.25f;
    [SerializeField] private float maxCurveLength = 5f;
    [SerializeField] private Transform aimPoint;
    [SerializeField] private LayerMask ignoreMask;

    private LineRenderer line;
    private bool visable;

    private void Awake()
    {
        line = GetComponent<LineRenderer>();
    }

    private void Update()
    {
        if(visable)
            DrawTrajectory();
    }

    public void SetBallisticValue(Vector3 startPosition, Vector3 velocity)
    {
        this.startPosition = startPosition;
        this.velocity = velocity;
        visable = true;
        aimPoint.gameObject.SetActive(true);
    }

    private void DrawTrajectory()
    {
        List<Vector3> curvePoints = new List<Vector3>();
        curvePoints.Add(startPosition);

        Vector3 currentPosition = startPosition;
        Vector3 currentVelocity = velocity;

        RaycastHit hit;
        Ray ray = new Ray(currentPosition, currentVelocity.normalized);
        while(!Physics.Raycast(ray, out hit, trajectoryVertDist, ignoreMask) && (startPosition - currentPosition).magnitude < maxCurveLength)
        {
            float t = trajectoryVertDist / currentVelocity.magnitude;
            currentVelocity = currentVelocity + t * Physics.gravity;
            currentPosition = currentPosition + t * currentVelocity;

            curvePoints.Add(currentPosition);
            ray = new Ray(currentPosition, currentVelocity.normalized);
        }

        if(hit.transform)
        {
            curvePoints.Add(hit.point);
            aimPoint.position = hit.point;
            aimPoint.rotation = Quaternion.FromToRotation(Vector3.forward, hit.normal);
        }
        else
        {
            aimPoint.position = curvePoints.LastOrDefault();
        }

        line.positionCount = curvePoints.Count;
        line.SetPositions(curvePoints.ToArray());
    }

    public void ClearTrajectory()
    {
        line.positionCount = 0;
        visable = false;
        aimPoint.gameObject.SetActive(false);
    }
}
