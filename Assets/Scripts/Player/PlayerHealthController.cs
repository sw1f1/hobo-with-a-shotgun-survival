﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealthController : HealthController
{
    [SerializeField] protected ValueStats food = new ValueStats(100, 100);
    [SerializeField] protected ValueStats water = new ValueStats(100, 100);

    public ValueStats Food => food;
    public ValueStats Water => water;
}
