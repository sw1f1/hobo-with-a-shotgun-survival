﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts
{
    [RequireComponent(typeof(Rigidbody))]
    public class CharacterMotor : MonoBehaviour
    {
        public bool sidescroller;

        private Vector3 currentSpeed;
        private float DistanceToTarget;
        private Rigidbody rb;
        private CapsuleCollider col;

        public Rigidbody Rigidbody { get { return rb; } }
        public CapsuleCollider Collider { get { return col; } }
        public Vector3 Velocity { get { return rb.velocity; } }

        private void Awake()
        {
            rb = GetComponent<Rigidbody>();
            col = GetComponent<CapsuleCollider>();

            Rigidbody.interpolation = RigidbodyInterpolation.Interpolate;
            if (sidescroller)
                Rigidbody.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionZ;
            else
                Rigidbody.constraints = RigidbodyConstraints.FreezeRotation;
            //add frictionless physics material
            if (Collider.material.name == "Default (Instance)")
            {
                PhysicMaterial pMat = new PhysicMaterial();
                pMat.name = "Frictionless";
                pMat.frictionCombine = PhysicMaterialCombine.Multiply;
                pMat.bounceCombine = PhysicMaterialCombine.Multiply;
                pMat.dynamicFriction = 0.1f;
                pMat.staticFriction = 0.1f;
                pMat.bounciness = 0f;
                Collider.material = pMat;
            }
        }

        public bool MoveTo(Vector3 destination, float acceleration, float stopDistance, bool ignoreY)
        {
            Vector3 relativePos = (destination - transform.position);
            if (ignoreY)
                relativePos.y = 0;

            DistanceToTarget = relativePos.magnitude;
            if (DistanceToTarget <= stopDistance)
                return true;
            else
                Rigidbody.AddForce(relativePos.normalized * acceleration * Time.deltaTime, ForceMode.VelocityChange);
            return false;
        }

        public void RotateToVelocity(float turnSpeed, bool ignoreY)
        {
            Vector3 dir;
            if (ignoreY)
                dir = new Vector3(Velocity.x, 0f, Velocity.z);
            else
                dir = Velocity;

            if (dir.magnitude > 0.1)
            {
                Quaternion dirQ = Quaternion.LookRotation(dir);
                Quaternion slerp = Quaternion.Slerp(transform.rotation, dirQ, dir.magnitude * turnSpeed * Time.deltaTime);
                Rigidbody.MoveRotation(slerp);
            }
        }

        public void RotateToDirection(Vector3 lookDir, float turnSpeed, bool ignoreY)
        {
            Vector3 characterPos = transform.position;
            if (ignoreY)
            {
                characterPos.y = 0;
                lookDir.y = 0;
            }

            Vector3 newDir = lookDir - characterPos;
            Quaternion dirQ = Quaternion.LookRotation(newDir);
            Quaternion slerp = Quaternion.Slerp(transform.rotation, dirQ, turnSpeed * Time.deltaTime);
            Rigidbody.MoveRotation(slerp);
        }

        public void ManageSpeed(float deceleration, float maxSpeed, bool ignoreY)
        {
            currentSpeed = Velocity;
            if (ignoreY)
                currentSpeed.y = 0;

            if (currentSpeed.magnitude > 0)
            {
                Rigidbody.AddForce((currentSpeed * -1) * deceleration * Time.deltaTime, ForceMode.VelocityChange);
                if (Velocity.magnitude > maxSpeed)
                    Rigidbody.AddForce((currentSpeed * -1) * deceleration * Time.deltaTime, ForceMode.VelocityChange);
            }
        }
    }
}
