﻿using Scripts.Items;
using Scripts.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestoryObject : MonoBehaviour, IHealth
{
    [SerializeField] private ValueStats health;
    [SerializeField] private Weapon.TypeMeleeWeapon typeWeapon;
    private ItemContainer container;

    public ValueStats Health => health;
    public Transform Transform => transform;

    private void Awake()
    {
        container = GetComponent<ItemContainer>();
    }

    public void OnDamage(float damage, FastItem currentWeapon, IHealth target)
    {
        Weapon weapon = (Weapon)ItemManager.GetItem(currentWeapon);

        if(weapon.TypeWeaponAttack == Weapon.TypeWeapon.Melee && (weapon.TypeMelee == Weapon.TypeMeleeWeapon.Both || weapon.TypeMelee == typeWeapon))
        {
            if (health.Value <= 0)
                return;

            StartCoroutine(Coroutines.Damage(transform, 10f, 2));

            if(container != null)
                InputManager.OnTakeItem(container.FItem);

            health.RemoveValue(damage);
            if (health.Value <= 0)
            {
                StartCoroutine(Coroutines.Dead(transform, 0f, () => Destroy(gameObject)));
            }
        }
    }
}
