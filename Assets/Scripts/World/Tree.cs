﻿using Scripts.Items;
using Scripts.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tree : MonoBehaviour, IHealth
{
    [SerializeField] private ValueStats health;

    private ItemContainer container;
    private Rigidbody rigidbody;

    public ValueStats Health => health;
    public Transform Transform => transform;

    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();
        container = GetComponent<ItemContainer>();
    }

    public void OnDamage(float damage, FastItem currentWeapon, IHealth target)
    {
        Weapon weapon = (Weapon)ItemManager.GetItem(currentWeapon);
        if (weapon.TypeWeaponAttack == Weapon.TypeWeapon.Melee && (weapon.TypeMelee == Weapon.TypeMeleeWeapon.Both || weapon.TypeMelee == Weapon.TypeMeleeWeapon.Tree))
        {
            if (health.Value <= 0)
                return;

            StartCoroutine(Coroutines.Damage(transform.parent, 10f, 2));

            InputManager.OnTakeItem(container.FItem);
            health.RemoveValue(damage);
            if (health.Value <= 0)
            {
                rigidbody.constraints = RigidbodyConstraints.None;

                Vector3 direction = (target.Transform.position - transform.position).normalized;
                rigidbody.AddTorque(direction * 10000f);

                StartCoroutine(Coroutines.Dead(transform, 5f, () => Destroy(gameObject)));
            }
        }
    }
}
