﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour, IUsable
{
    public enum DoorType {Open, Close}

    [SerializeField] private Transform door;
    [SerializeField] private DoorType type;
    [SerializeField] private float speedMove = 10f;

    [SerializeField] private string textUse = "Use";
    [SerializeField] private float timeUsed = 0f;

    private Quaternion rotation;
    private bool isActive = true;
    public bool IsActive => isActive;

    public float TimeUsed => timeUsed;

    public string TextUse => textUse;

    public bool CombineWithOther { get => false; set => throw new NotImplementedException(); }
    public Action<IUsable> OnUsed { get; set; }

    public void OnClickUse()
    {
        if(type == DoorType.Open)
        {
            rotation = Quaternion.Euler(0, 0, 0);
            type = DoorType.Close;
            
        }else
        {
            rotation = Quaternion.Euler(0, 120, 0);
            type = DoorType.Open;
        }
    }

    public void OnUse()
    {
        
    }

    public void Show(bool value)
    {
        
    }

    private void Update()
    {
        door.localRotation = Quaternion.Slerp(door.localRotation, rotation, Time.deltaTime * speedMove);
    }
}
