﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ladder : MonoBehaviour
{
    [SerializeField] private Vector3 center;
    [SerializeField] private Vector3 forward;

    [SerializeField] private Transform parent;

    public Vector3 Center => transform.position + parent.rotation * center;
    public Vector3 Forward => parent.rotation * forward;


    private void Awake()
    {
        parent = transform.parent;
    }


    private void OnDrawGizmos()
    {
        if (parent == null)
            return;
        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(Center, 0.2f);
        Gizmos.DrawRay(transform.position, Forward);
    }
}
