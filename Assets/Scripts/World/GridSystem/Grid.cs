﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts.World.GridSystem
{
    public class Grid<TGridObject>
    {

        public event EventHandler<OnGridValueChangedEventArgs> OnGridValueChanged;
        public class OnGridValueChangedEventArgs : EventArgs
        {
            public int x;
            public int y;
        }

        private int width;
        private int height;
        private float cellSize;
        private Vector3 originPosition;
        private TGridObject[,] array;

        public float CellSize => cellSize;

        public Grid(int width, int height, float cellSize, Vector3 originPosition, Func<Grid<TGridObject>, int, int, TGridObject> createGridObject)
        {
            this.width = width;
            this.height = height;
            this.cellSize = cellSize;
            this.originPosition = originPosition;

            array = new TGridObject[width, height];
            for (int x = 0; x < width; x++)
                for (int y = 0; y < height; y++)
                {
                    array[x, y] = createGridObject(this, x, y);
                }
        }

        public Vector3 GetWorldPosition(int x, int y)
        {
            return new Vector3(x, 0, y) * cellSize + originPosition;
        }

        public Vector3 GetWorldPositionCenterCell(int x, int y)
        {
            return GetWorldPosition(x, y) + new Vector3(cellSize / 2, 0, cellSize / 2);
        }

        public Vector2Int GetGridObjectPosition(Vector3 worldPosition)
        {
            Vector2Int position = Vector2Int.zero;
            position.x = Mathf.FloorToInt((worldPosition - originPosition).x / cellSize);
            position.y = Mathf.FloorToInt((worldPosition - originPosition).z / cellSize);
            return position;
        }

        public Vector3 WorldToGridPosition(Vector3 worldPosition)
        {
            Vector2Int gridPosition = GetGridObjectPosition(worldPosition);
            return GetWorldPosition(gridPosition.x, gridPosition.y);
        }

        public Vector3 WorldToGridCenterPosition(Vector3 worldPosition)
        {
            Vector2Int gridPosition = GetGridObjectPosition(worldPosition);
            return GetWorldPositionCenterCell(gridPosition.x, gridPosition.y);
        }

        public void SetGridObject(int x, int y, TGridObject value)
        {
            if(x >= 0 && y >= 0 && x < width && y < height)
            {
                array[x, y] = value;
                TriggerGridObjectChanged(x,y);
            }
        }

        public void TriggerGridObjectChanged(int x, int y)
        {
            if(OnGridValueChanged != null)
            OnGridValueChanged(this, new OnGridValueChangedEventArgs { x = x, y = y });
        }

        public void SetGridObject(Vector3 worldPosition, TGridObject value)
        {
            Vector2Int position = GetGridObjectPosition(worldPosition);
            SetGridObject(position.x, position.y, value);
        }

        public TGridObject GetGridObject(int x, int y)
        {
            if (x >= 0 && y >= 0 && x < width && y < height)
            {
                return array[x, y];
            }else
            {
                return default(TGridObject);
            }
        }

        public TGridObject GetGridObject(Vector2Int position)
        {
            return GetGridObject(position.x, position.y);
        }

        public TGridObject GetGridObject(Vector3 worldPosition)
        {
            Vector2Int position = GetGridObjectPosition(worldPosition);
            return GetGridObject(position.x, position.y);
        }
    }
}
