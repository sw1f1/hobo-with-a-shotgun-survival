﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Scripts.Managers;

namespace Scripts.World.GridSystem
{
    public class GridManager : Singleton<GridManager>
    {
        [SerializeField] private int width = 5;
        [SerializeField] private int height = 5;
        [SerializeField] private float cellSize = 1f;
        [SerializeField] private Vector3 originPosition = Vector3.zero;

        [Space]
        [SerializeField] private bool drawGizmos = false;

        private Grid<GridObject> grid;

        public static Grid<GridObject> Grid => Instance.grid;
        public Vector3 OriginPosition => originPosition;
        public int Width => width;
        public int Height => height;
        public float CellSize => cellSize;

        protected override void Awake()
        {
            base.Awake();
            grid = new Grid<GridObject>(width, height, cellSize, originPosition, 
                (Grid<GridObject> grid, int x, int y) => new GridObject(grid, x, y));
        }

        private void OnDrawGizmos()
        {
            if (!drawGizmos)
                return;

            if (grid == null)
                return;

            for (int x = 0; x< width; x++)
                for(int y = 0; y < height; y++)
                {
                    Gizmos.color = Color.black;
                    Gizmos.DrawWireCube(grid.GetWorldPositionCenterCell(x,y), new Vector3(cellSize, 0.2f, cellSize));

                    Gizmos.color = Color.grey;
                    Gizmos.DrawSphere(grid.GetWorldPositionCenterCell(x, y), 0.1f);
                }
        }
    }
}
