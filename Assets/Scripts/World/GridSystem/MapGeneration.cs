﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts.World.GridSystem
{
    public class MapGeneration : MonoBehaviour
    {
        [SerializeField] private List<SpawnObject> spawnObjects = new List<SpawnObject>();

        private GridManager gridManager;

        private void Awake()
        {
            gridManager = GetComponent<GridManager>();
        }

        private void Start()
        {
            StartCoroutine(OnGenerateObjects());
        }

        private IEnumerator OnGenerateObjects()
        {
            foreach (var obj in spawnObjects)
            {
                OnSpawnObject(obj);
                yield return new WaitForEndOfFrame();
            }
        }

        private void OnSpawnObject(SpawnObject spawnObject)
        {
            GameObject parent = new GameObject();
            parent.name = spawnObject.Name;
            parent.transform.SetParent(transform);

            for (int i = 0; i < spawnObject.Count; i++)
            {
                Vector3 position = gridManager.OriginPosition + gridManager.CellSize * new Vector3(Random.Range(0, gridManager.Width), 0, Random.Range(0, gridManager.Height));
                GameObject go = Instantiate(spawnObject.Prefab, position, Quaternion.Euler(0, Random.Range(0, 360), 0), parent.transform);

                Vector2Int posOnGrid = GridManager.Grid.GetGridObjectPosition(position);
                GridManager.Grid.GetGridObject(posOnGrid).AddGameObject(go, Items.Build.Layers.Object, Items.Build.Direction.Down, 0);
            }
        }
    }
}
