﻿using Scripts.Items;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts.World.GridSystem
{
    [System.Serializable]
    public class GridObject
    {
        private static int COUNTSIZE = 3;

        private GameObject[] floors;
        private WallObject[] walls;
        private GameObject[] objects;

        private Grid<GridObject> grid;
        private int x;
        private int y;

        public bool IsFree(Build.Layers layer, Build.Direction direction, int level)
        {
            switch (layer)
            {
                default:
                case Build.Layers.Floor:
                    return floors[level] == null;
                case Build.Layers.Wall:
                    return walls[level].IsFree(direction);
                case Build.Layers.Object:
                    return objects[level] == null;
            }
        }

        public bool IsFloor(int level)
        {
            return floors[level] != null;
        }

        public bool IsWallLevelDown(int level, Build.Direction direction)
        {
            int levelDown = level - 1;
            if (levelDown < 0)
                return floors[0] != null;

            return !walls[levelDown].IsFree(direction);
        }

        public GridObject(Grid<GridObject> grid, int x, int y)
        {
            floors = new GameObject[COUNTSIZE + 1];
            walls = new WallObject[COUNTSIZE];
            objects = new GameObject[COUNTSIZE];

            for(int i = 0; i < COUNTSIZE; i++)
            {
                walls[i] = new WallObject();
            }

            this.grid = grid;
            this.x = x;
            this.y = y;
        }

        public void AddGameObject(GameObject go, Build.Layers layer, Build.Direction direction, int level)
        {
            switch(layer)
            {
                default:
                case Build.Layers.Floor:
                    floors[level] = go;
                    break;
                case Build.Layers.Wall:
                    walls[level].AddWall(go, direction);
                    break;
                case Build.Layers.Object:
                    objects[level] = go;
                    break;
            }

            grid.TriggerGridObjectChanged(x, y);
        }

        public void RemoveGameObject(Build.Layers layer, Build.Direction direction, int level)
        {
            switch (layer)
            {
                default:
                case Build.Layers.Floor:
                    floors[level] = null;
                    break;
                case Build.Layers.Wall:
                    walls[level].RemoveWall(direction);
                    break;
                case Build.Layers.Object:
                    objects[level] = null;
                    break;
            }
        }
    }

    public class WallObject
    {
        private GameObject[] walls;

        public WallObject()
        {
            walls = new GameObject[4];
        }

        public bool IsFree(Build.Direction direction)
        {
            return walls[(int)direction] == null;
        }

        public void AddWall(GameObject wall, Build.Direction direction)
        {
            walls[(int)direction] = wall;
        }

        public void RemoveWall(Build.Direction direction)
        {
            walls[(int)direction] = null;
        }

    }
}
