﻿using Scripts.Items;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts.World.GridSystem
{
    public class BuildObject : MonoBehaviour
    {
        [SerializeField] private Vector3 center = new Vector3(0, 0, 0);
        [SerializeField] private Vector3 halfExstens = new Vector3(1, 1, 1);
        [SerializeField] private List<Collider> colliders = new List<Collider>();
        [SerializeField] private Build.Direction direction;

        private bool canBuild = false;
        private bool isBuilded = false;

        public void OnBuild()
        {
            foreach(var col in colliders)
            {
                col.enabled = false;
            }
        }

        public void OnBuilded()
        {
            isBuilded = true;
            foreach (var col in colliders)
            {
                col.enabled = true;
            }
        }

        public bool CheckBox(LayerMask mask, Build.Direction direction)
        {
            this.direction = direction;
            return Physics.CheckBox(transform.position + CalculateCenter(), CalculateHalfExstens(), transform.rotation, mask);
        }

        public void VisualCheckBuild(bool value)
        {
            canBuild = value;
        }

        private void OnDrawGizmos()
        {
            if (isBuilded)
                return;

            if (canBuild)
                Gizmos.color = Color.green;
            else
                Gizmos.color = Color.red;

            Gizmos.DrawWireCube(transform.position + CalculateCenter(), CalculateHalfExstens() * 2f);
        }

        private Vector3 CalculateHalfExstens()
        {
            switch (direction)
            {
                default:
                case Build.Direction.Down: return halfExstens;
                case Build.Direction.Left: return new Vector3(halfExstens.z, halfExstens.y, halfExstens.x);
                case Build.Direction.Up: return new Vector3(halfExstens.z, halfExstens.y, halfExstens.x);
                case Build.Direction.Right: return halfExstens;
            }
        }

        private Vector3 CalculateCenter()
        {
            switch (direction)
            {
                default:
                case Build.Direction.Down: return center;
                case Build.Direction.Left: return new Vector3(center.z, center.y, -center.x);
                case Build.Direction.Up: return new Vector3(-center.z, center.y, center.x);
                case Build.Direction.Right: return new Vector3(-center.x, center.y, -center.z);
            }
        }
    }
}
