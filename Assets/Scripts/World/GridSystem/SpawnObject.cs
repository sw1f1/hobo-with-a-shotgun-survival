﻿using Scripts.Utility;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts.World.GridSystem
{
    [System.Serializable]
    public class SpawnObject
    {
        [SerializeField] private string name;
        [SerializeField] private GameObject[] prefabs;
        [SerializeField] private Count count;

        public string Name => name;
        public GameObject Prefab => prefabs[Random.Range(0, prefabs.Length)];
        public int Count => count.GetInt();
    }
}
