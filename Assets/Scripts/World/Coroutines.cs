﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Coroutines
{
    public static IEnumerator Damage(Transform transform, float speed, int iteration)
    {
        for (int i = 0; i < iteration; i++)
        {
            while ((transform.localScale - new Vector3(1.2f, 1.2f, 1.2f)).magnitude > 0.1f)
            {
                yield return null;
                transform.localScale = Vector3.Lerp(transform.localScale, new Vector3(1.2f, 1.2f, 1.2f), Time.deltaTime * speed);
            }

            while ((transform.localScale - Vector3.one).magnitude > 0.1f)
            {
                yield return null;
                transform.localScale = Vector3.Lerp(transform.localScale, Vector3.one, Time.deltaTime * speed);
            }
        }
        transform.localScale = Vector3.one;
    }

    public static IEnumerator Dead(Transform transform, float timeWait, Action callback)
    {
        yield return new WaitForSeconds(timeWait);
        float time = 5f;

        Rigidbody rigidbody = transform.GetComponent<Rigidbody>();
        if(rigidbody != null)
            rigidbody.isKinematic = true;

        while (time > 0)
        {
            yield return null;
            time -= Time.deltaTime;
            transform.position += Vector3.down * Time.deltaTime * 4f;
        }

        callback?.Invoke();
    }
}
