﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectMap : MonoBehaviour
{
    [SerializeField] private Sprite sprite;
    [SerializeField] private Color color;

    [SerializeField] private bool updateRotation;
    [SerializeField] private float size = 5f;
    [SerializeField] private int layer;
    [SerializeField] private bool inBound;

    public Sprite Sprite => sprite;
    public Color Color => color;
    public bool UpdateRotation => updateRotation;
    public float Size => size;
    public int Layer => layer;
    public bool InBound => inBound;

    private void Start()
    {
        EventManager.OnSetObjectOnMap?.Invoke(this);
    }

    private void OnDestroy()
    {
        EventManager.OnDestroyObjectOnMap?.Invoke(this);
    }
}
