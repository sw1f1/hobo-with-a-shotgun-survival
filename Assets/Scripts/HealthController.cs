﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthController : MonoBehaviour, IHealth
{
    [SerializeField] protected ValueStats health = new ValueStats(100,100);

    public ValueStats Health => health;

    public Transform Transform => transform;

    private void OnEnable()
    {
        health.OnChange += OnDead;
    }

    private void OnDisable()
    {
        health.OnChange -= OnDead;
    }

    public void SetHealthOnTime(float value, float time)
    {
        StartCoroutine(ChangeHealthOnTime(value, time));
    }

    private IEnumerator ChangeHealthOnTime(float value, float time)
    {
        float t = 0;
        while(t < time)
        {
            health.RemoveValue(value);
            t += Time.deltaTime;
            yield return null;
        }
    }

    protected virtual void OnDead(ValueStats value)
    {

    }

    public void OnDamage(float damage, FastItem currentWeapon, IHealth other)
    {
        if (other.Equals(this))
            return;

        health.RemoveValue(damage);
    }
}

[System.Serializable]
public class ValueStats
{
    public Action<ValueStats> OnChange;
    [SerializeField] private float value = 100f;
    [SerializeField] private float maxValue = 100f;

    public float Value => value;
    public float MaxValue => maxValue;

    public ValueStats(float maxValue)
    {
        this.value = maxValue;
        this.maxValue = maxValue;
    }

    public ValueStats(float value, float maxValue)
    {
        this.value = value;
        this.maxValue = maxValue;
    }

    public void SetValues(float value, float maxValue)
    {
        this.value = value;
        this.maxValue = maxValue;
        OnChange?.Invoke(this);
    }

    public void AddValue(float value)
    {
        this.value += value;
        this.value = Mathf.Clamp(this.value, 0, maxValue);
        OnChange?.Invoke(this);
    }

    public void RemoveValue(float value)
    {
        this.value -= value;
        this.value = Mathf.Clamp(this.value, 0, maxValue);
        OnChange?.Invoke(this);
    }
}
