﻿using System;
using Scripts.Items;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BerryBush : MonoBehaviour, IUsable
{
    [SerializeField] private GameObject emptyBerry;
    [SerializeField] private GameObject berry;
    [SerializeField] private bool combineWithOther;
    [SerializeField] private string textUse;
    [SerializeField] private float timeUsed = 1f;

    private bool isActive = true;
    public bool IsActive => isActive;

    public bool CombineWithOther { get { return combineWithOther; } set { combineWithOther = value; } }
    public Transform Transform { get { return transform; } }
    public string TextUse => textUse;
    public float TimeUsed => timeUsed;

    private Renderer render;
    private ItemContainer container;
    private bool used = false;
    public Action<IUsable> OnUsed { get; set; }

    public void Start()
    {
        container = GetComponent<ItemContainer>();
        render = GetComponentInChildren<Renderer>();
        Show(false);
        SetRandomBerry();
    }

    private void SetRandomBerry()
    {
        if(UnityEngine.Random.Range(0f, 1f) > 0.5f)
        {
            SwitchBerryMesh();
        }
    }

    public void Show(bool value)
    {
        render.sharedMaterial.SetInt("OUTLINES", 1);
    }

    public void OnClickUse()
    {
        if (used)
            return;

        EventManager.OnVisibleUseObject?.Invoke(false, null);
        used = true;
    }

    public void OnUse()
    {
        InputManager.OnTakeItem(container.FItem);

        OnUsed?.Invoke(this);

        used = false;
        SwitchBerryMesh();
    }

    private void SwitchBerryMesh()
    {
        isActive = !isActive;
        emptyBerry.SetActive(!emptyBerry.activeSelf);
        berry.SetActive(!berry.activeSelf);
    }
}
