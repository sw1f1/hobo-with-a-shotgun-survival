﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomMesh : MonoBehaviour
{
    [SerializeField] private GameObject[] meshes;

    private void Awake()
    {
        if(meshes != null && meshes.Length > 0)
        {
            meshes[Random.Range(0, meshes.Length)].SetActive(true);
        }
    }
}
