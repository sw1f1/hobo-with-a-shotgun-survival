﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Scripts.Items;
using Scripts.Managers;
using Scripts.MovementController;
using Scripts.World.GridSystem;

public class InputManager : Singleton<InputManager>
{
    public static Action<PlayerInventory> OnClickInventory;
    public static Action<PlayerInventory, BaseInventory> OnClickUseInventory;
    public static Action OnClickUse;
    public static Action OnPointerDownAttack;
    public static Action OnPointerUpAttack;
    public static Action OnReloadWeapon;
    public static Action<Vector2> OnScroll;

    [Header("Build")]
    [SerializeField] private Item buildItem;

    [Header("Movement")]
	[SerializeField] private string horizontalInputAxis = "Horizontal";
	[SerializeField] private string verticalInputAxis = "Vertical";
	[SerializeField] private KeyCode jumpKey = KeyCode.Space;
	[SerializeField] private KeyCode crouchKey = KeyCode.LeftControl;
    [SerializeField] private KeyCode sprintKey = KeyCode.LeftShift;
    [SerializeField] private KeyCode useFoodKey = KeyCode.Q;
    [SerializeField] private KeyCode useThrowKey = KeyCode.G;
    [SerializeField] private KeyCode buildMenuKey = KeyCode.B;

    [Header("Camera")]
    [SerializeField] private string mouseHorizontalAxis = "Mouse X";
    [SerializeField] private string mouseVerticalAxis = "Mouse Y";

    [Range(0.1f, 10f)]
    [SerializeField] private float mouseSensitivity = 2.0f;

    [Header("States")]
    [SerializeField] private bool pause;
    [SerializeField] private bool inventory;
    [SerializeField] private bool isAttack;
    [SerializeField] private bool isCrouch = false;
    [SerializeField] private bool isBuilding = false;

    public static bool IsAttack => Instance.isAttack;

    private void Update() 
    {
        if(Input.GetKeyDown(KeyCode.Escape) && inventory)
        {
            CloseInvemtory();
        }

        if(Input.GetKeyDown(KeyCode.Tab))
        {
            inventory = !inventory;
            pause = !pause;
            OnClickInventory?.Invoke(PlayerStats.Inventory);
        }

        if(Input.GetKeyDown(KeyCode.E) && !pause)
        {
            OnClickUse?.Invoke();
        }

        if(Input.GetKeyDown(KeyCode.Mouse0) && !pause)
        {
            isAttack = true;
            OnPointerDownAttack?.Invoke();
        }

        if(Input.GetKeyUp(KeyCode.Mouse0) && !pause)
        {
            isAttack = false;
            OnPointerUpAttack?.Invoke();
        }

        if(Input.GetKeyUp(KeyCode.R) && !pause)
        {
            OnReloadWeapon?.Invoke();
        }

        if(Input.GetKeyDown(KeyCode.Alpha1))
        {
            SwitchWeapon(0);
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            SwitchWeapon(1);
        }

        if(Input.mouseScrollDelta != Vector2.zero && !isBuilding)
        {
            SwitchWeapon();
        }

        if(Input.mouseScrollDelta != Vector2.zero)
            OnScroll?.Invoke(Input.mouseScrollDelta);

        if (Input.GetKeyDown(useFoodKey))
        {
            UseFoodItem();
        }

        if (Input.GetKeyDown(useThrowKey))
        {
            UseThrowItem();
        }

        if(Input.GetKeyDown(buildMenuKey))
        {
            UseBuildItem();
        }

        if (pause && isAttack)
        {
            isAttack = false;
            OnPointerUpAttack?.Invoke();
        }
    }

    public static void CloseInvemtory()
    {
        Instance.inventory = false;
        Instance.pause = false;
        OnClickInventory?.Invoke(null);
    }

    public static void OpenOtherInventory(BaseInventory other)
    {
        Instance.inventory = !Instance.inventory;
        Instance.pause = !Instance.pause;
        OnClickUseInventory?.Invoke(PlayerStats.Inventory, other);
    }

    public static void OnTakeItem(FastItem item)
    {
        PlayerStats.Inventory.AddItem(item);
        EventManager.OnViewItem(item);
    }

    public static bool IsJumpKeyPressed()
	{
        if(Instance.pause)
            return false;
		return Input.GetKeyDown(Instance.jumpKey);
	}

    public static bool IsSprintKeyPressed()
    {
        if (Instance.pause)
            return false;
        return Input.GetKey(Instance.sprintKey);
    }

    public static bool IsAimKeyPressed()
    {
        if (Instance.pause)
            return false;
        return Input.GetKey(KeyCode.Mouse1);
    }

    public static bool IsCrouchKeyPressed()
	{
        if(Instance.pause)
            return false;

        bool crouch =  Input.GetKeyDown(Instance.crouchKey);
        if(crouch)
        {
            Instance.isCrouch = !Instance.isCrouch;
        }
		return crouch;
	}

    public static float GetHorizontalMovementInput()
	{
        if(Instance.pause)
            return 0;
        return Input.GetAxisRaw(Instance.horizontalInputAxis);
	}

	public static float GetVerticalMovementInput()
	{
        if(Instance.pause)
            return 0;

        return Input.GetAxisRaw(Instance.verticalInputAxis);
	}

    public static float GetHorizontalCameraInput()
    {
         if(Instance.pause)
            return 0;
        return Input.GetAxisRaw(Instance.mouseHorizontalAxis) * Instance.mouseSensitivity;
    }

    public static float GetVerticalCameraInput()
    {
         if(Instance.pause)
            return 0;
        return -Input.GetAxisRaw(Instance.mouseVerticalAxis) * Instance.mouseSensitivity;
    }

    public static void SwitchWeapon()
    {
        if (Instance.pause)
            return;

        if (Instance.isBuilding)
        {
            Instance.isBuilding = false;
            EventManager.OnCloseBuildPanel?.Invoke();
        }

        PlayerStats.Inventory.SwitchCurentWeapon();
    }

    public static void SwitchWeapon(int index)
    {
        if (Instance.pause)
            return;

        if (Instance.isBuilding)
        {
            Instance.isBuilding = false;
            EventManager.OnCloseBuildPanel?.Invoke();
        }

        PlayerStats.Inventory.SwitchWeapon(index);
    }

    public static void UseFoodItem()
    {
        if (Instance.pause)
            return;

        PlayerStats.Inventory.UseEqupmentFood();
    }

    public static void UseThrowItem()
    {
        if (Instance.pause)
            return;

        if (GridManager.Instance == null)
            return;

        FastItem item = PlayerStats.Inventory.GetEquipItem(3);
        if (item == null || item.Equals(FastItem.None))
            return;

        PlayerStats.PlayerGO.GetComponent<WeaponController>().SetWeapon(item, PlayerStats.Inventory);
    }

    public static void UseBuildItem()
    {
        if (Instance.pause)
            return;

        if(Instance.isBuilding)
        {
            SwitchWeapon(0);
            return;
        }

        FastItem item = Instance.buildItem.GetFastItem();
        if (item == null || item.Equals(FastItem.None))
            return;

        Instance.isBuilding = true;
        PlayerStats.PlayerGO.GetComponent<WeaponController>().SetWeapon(item, PlayerStats.Inventory);
        EventManager.OnOpenBuildPanel?.Invoke(PlayerStats.Inventory);
    }

    public static float GetScatterCount()
    {
        float scatter = 0;
        float crouch = (Instance.isCrouch) ? 5f : 0f;
        if(GetHorizontalMovementInput() != 0 || GetVerticalMovementInput() != 0)
        {
            scatter += 10f - crouch;
        }

        scatter += (Mathf.Abs(GetHorizontalCameraInput()) + Mathf.Abs(GetVerticalCameraInput())) * 3f;

        return scatter;
    }    
}
