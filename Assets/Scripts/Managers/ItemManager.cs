﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Scripts.Items;
using System.Linq;

namespace Scripts.Managers
{
    public class ItemManager : Singleton<ItemManager>
    {
        [SerializeField] private List<Item> items = new List<Item>();

        private HashSet<Item> hashItems = new HashSet<Item>();

        public static Item[] Items => Instance.hashItems.ToArray();

        protected override void Awake()
        {
            base.Awake();
            foreach(var item in items)
            {
                hashItems.Add(item);
            }
        }

        public static Item GetItem(FastItem fitem)
        {
            if (fitem == null)
                return null;

            return Instance.hashItems.FirstOrDefault(x => x.GetHashCode() == fitem.ID);
        }

        public static Ammo GetAmmo(Ammo.TypeAmmo type)
        {
            return Instance.hashItems.FirstOrDefault(x => (x as Ammo) != null 
                    && (x as Ammo).AmmoType == type) as Ammo;
        }

        public static Item GetRandomItem()
        {
            return Items[Random.Range(0, Items.Length)];
        }
    }
}
