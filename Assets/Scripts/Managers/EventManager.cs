﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class EventManager
{
    public static Action<bool, IUsable> OnVisibleUseObject;
    public static Action<IUsable> OnUsedObject;

    //HUD
    public static Action<FastItem> OnViewItem;
    public static Action<bool> OnSetActiveScopeAIM;
    public static Action<PlayerInventory> OnOpenBuildPanel;
    public static Action OnCloseBuildPanel;
    public static Action<FastItem> OnSelectBuildItem;

    //Map
    public static Action<ObjectMap> OnSetObjectOnMap;
    public static Action<ObjectMap> OnDestroyObjectOnMap;
}
