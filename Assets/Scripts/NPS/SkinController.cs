﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts.NPS
{
    public class SkinController : MonoBehaviour
    {
        [SerializeField] private GameObject[] heads;
        [SerializeField] private GameObject[] bodys;
        [SerializeField] private GameObject[] legs;

        [SerializeField] private GameObject head;
        [SerializeField] private GameObject body;
        [SerializeField] private GameObject leg;

        private void Awake()
        {
            RefreshSkin();
        }

        [ContextMenu("RefreshSkin")]
        public void RefreshSkin()
        {
            SetActiveSkin(false);

            if(heads != null && heads.Length != 0)
                head = heads[Random.Range(0, heads.Length)];
            if (bodys != null && bodys.Length != 0)
                body = bodys[Random.Range(0, bodys.Length)];
            if (legs != null && legs.Length != 0)
                leg = legs[Random.Range(0, legs.Length)];

            SetActiveSkin(true);
        }

        private void SetActiveSkin(bool value)
        {
            if (head != null)
                head.SetActive(value);
            if (body != null)
                body.SetActive(value);
            if (leg != null)
                leg.SetActive(value);
        }
    }
}
