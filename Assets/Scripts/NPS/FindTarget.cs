﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts.NPS
{
    public class FindTarget : MonoBehaviour
    {
        [SerializeField] private bool drawFov = false;
        [Space()]
        [SerializeField] private float distanceVision = 10f;
        [SerializeField] private float distanceLoss = 15f;
        [SerializeField] private int fov = 30;

        public float DistanceVision=> distanceVision;
        public float DistanceLoss => distanceLoss;
        public int Fov => fov;

        //private INps nps;

        private void Awake()
        {
           // nps = GetComponent<INps>();
        }

        public Transform Target()
        {
            Collider[] colliders = Physics.OverlapSphere(transform.position, DistanceVision);
            foreach (var collider in colliders)
            {
               // if (collider.GetComponent<INps>() != null && collider.GetComponent<INps>().Type != nps.Type)
                {
                    Vector3 direction = collider.transform.position - transform.position;
                    float dot = Vector3.Dot(transform.forward, direction.normalized);
                    float fov = Fov / 90f;
                    if (fov <= dot)
                    {
                        return collider.transform;
                    }
                }
            }

            return null;
        }

        private void OnDrawGizmos()
        {
            if (!drawFov) return;

            Gizmos.color = Color.red;

            for (int angle = -Fov; angle <= Fov; angle++)
            {
                Vector3 dir = Quaternion.AngleAxis(angle, Vector3.up) * transform.forward;
                Gizmos.DrawLine(transform.position, transform.position + dir * DistanceVision);
            }

            Gizmos.color = Color.blue;
            for (int angle = -Fov; angle <= Fov; angle++)
            {
                Vector3 dir = Quaternion.AngleAxis(angle, Vector3.up) * transform.forward;
                Gizmos.DrawLine(transform.position + dir * DistanceVision, transform.position + dir * DistanceLoss);
            }
        }
    }
}
