﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using Scripts.Managers;

public class MainUI : Singleton<MainUI>
{
    [SerializeField] private List<UIMenu> menus = new List<UIMenu>();
    [SerializeField] private List<PanelUI> panels = new List<PanelUI>();
    [SerializeField] private UIMenu currentMenu;

    [Header("FPS")]
    [SerializeField] private bool showFps = true;
    [SerializeField] private Text fpsText;
    private float deltaTime;


    protected override void Awake()
    {
        base.Awake();
        Application.targetFrameRate = 120;
        QualitySettings.vSyncCount = 0;
        currentMenu.Open();
    }

    private void OnEnable() 
    {
        InputManager.OnClickInventory += OnClickInventory;
        InputManager.OnClickUseInventory += OnOpenOtherInventory;
    }

    private void OnDisable() 
    {
        InputManager.OnClickInventory -= OnClickInventory;
        InputManager.OnClickUseInventory -= OnOpenOtherInventory;
    }

    private void Start()
    {
        if (fpsText != null)
            fpsText.gameObject.SetActive(showFps);
    }

    private void Update()
    {
        if (showFps && fpsText != null)
        {
            deltaTime += (Time.deltaTime - deltaTime) * 0.1f;
            float fps = 1.0f / deltaTime;
            fpsText.text = Mathf.Ceil(fps).ToString();
        }
    }

    public static T GetMenu<T>() where T : UIMenu
    {
        UIMenu panel = Instance.menus.FirstOrDefault(x => x.GetComponent<T>() != null);
        return (T)panel;
    }

    public static T OpenMenu<T>() where T : UIMenu
    {
        if(Instance.currentMenu != null)
            Instance.currentMenu.Close();

        T panel = GetMenu<T>();
        panel.Open();
        Instance.currentMenu = panel;
        return panel;
    }

    public static void CloseMenu<T>() where T : UIMenu
    {
        T panel = GetMenu<T>();
        panel.Close();
    }

    public static T GetPanel<T>() where T : PanelUI
    {
        PanelUI panel = Instance.panels.FirstOrDefault(x => x.GetComponent<T>() != null);
        return (T)panel;
    }

    public static T OpenPanel<T>() where T : PanelUI
    {
        T panel = GetPanel<T>();
        panel.Open();

        return panel;
    }

    public static void ClosePanel<T>() where T : PanelUI
    {
        T panel = GetPanel<T>();
        panel.Close();
    }

    public static void CloseCurrentMenu()
    {
        Instance.currentMenu.Close();
    }

    public void OnClickInventory(PlayerInventory plater)
    {
        if(currentMenu as InventoryMenu)
        {
            OpenMenu<HUDUI>();
        }else
        {
            InventoryMenu menu = OpenMenu<InventoryMenu>(); 
            menu.Open(plater);
        }
    }

    public void OnOpenOtherInventory(PlayerInventory plater, BaseInventory other)
    {
        InventoryMenu menu = OpenMenu<InventoryMenu>(); 
        menu.Open(plater, other);
    }
}
