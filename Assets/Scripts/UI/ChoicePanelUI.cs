﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChoicePanelUI : PanelUI
{
    [SerializeField] private Text text;

    private Action callback;

    public void Open(string text, Action callback)
    {
        this.text.text = text;
        this.callback = callback;
        Open();
    }

    public void OnClickYes()
    {
        Close();
        callback?.Invoke();
    }

    public void OnClickNo()
    {
        Close();
    }
}
