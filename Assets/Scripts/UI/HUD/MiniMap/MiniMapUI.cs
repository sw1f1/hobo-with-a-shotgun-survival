﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Scripts.Managers;

namespace Scripts.UI.MiniMap
{
    public class MiniMapUI : MonoBehaviour
    {
        public enum GameMode {RotatePlayerImage, RotateMapImage };

        [SerializeField] private RectTransform player;
        [SerializeField] private GameMode mode = GameMode.RotateMapImage;
        [SerializeField] private float zoom = 10f;
        [SerializeField] private RectTransform[] layers;

        [SerializeField] private MinimapBlip minimapBlipPrefab;
        [SerializeField] private RectTransform inBoundsRect;

        private List<MinimapBlip> blips = new List<MinimapBlip>();

        private Vector2 xRot;
        private Vector2 yRot;
        private float rotateValue;
        private Rect rect;

        private Transform playerModel;

        public float Zoom => zoom;

        private void Awake()
        {
            rect = GetComponent<RectTransform>().rect;

            EventManager.OnSetObjectOnMap += OnSetObjectOnMap;
            EventManager.OnDestroyObjectOnMap += OnDestoyObjectMap;
        }

        private void OnDestroy()
        {
            EventManager.OnSetObjectOnMap -= OnSetObjectOnMap;
            EventManager.OnDestroyObjectOnMap -= OnDestoyObjectMap;
        }

        private void Start()
        {
            playerModel = PlayerStats.PlayerGO.transform.GetChild(0);
        }

        public Vector3 TransformPosition(Vector3 position)
        {
            Vector3 offset = position - playerModel.position;
            Vector2 pos = offset.x * xRot;
            pos += offset.z * yRot;
            pos *= zoom;
            return pos;
        }

        public Vector3 TransformRotation(Vector3 rotation)
        {
            return new Vector3(0,0, rotateValue - rotation.y);
        }

        public Vector2 MoveInside(Vector2 point)
        {
            point = Vector2.ClampMagnitude(point, rect.width / 2);
            return point;
        }

        private void OnSetObjectOnMap(ObjectMap objectMap)
        {
            var blip = Instantiate(minimapBlipPrefab);
            if (objectMap.InBound)
            {
                blip.GetComponent<RectTransform>().SetParent(inBoundsRect);
            }
            else
            {
                blip.GetComponent<RectTransform>().SetParent(layers[objectMap.Layer]);
            }

            blip.Init(this, objectMap);
            blips.Add(blip);
        }

        private void OnDestoyObjectMap(ObjectMap objectMap)
        {
            var blip = blips.Where(x=> x.CheckObjectMap(objectMap)).FirstOrDefault();
            if(blip != null)
            {
                blips.Remove(blip);
                Destroy(blip.gameObject);
            }
        }

        private void LateUpdate()
        {
            if(mode == GameMode.RotateMapImage)
            {
                xRot = new Vector2(playerModel.right.x, -playerModel.right.z);
                yRot = new Vector2(-playerModel.forward.x, playerModel.forward.z);
                rotateValue = playerModel.eulerAngles.y;
                player.localEulerAngles = Vector3.zero;

            }
            else if (mode == GameMode.RotatePlayerImage)
            {
                xRot = Vector2.right;
                yRot = Vector2.up;
                rotateValue = 0;
                player.localEulerAngles = new Vector3(0, 0, playerModel.eulerAngles.y);
            }
        }
    }
}
