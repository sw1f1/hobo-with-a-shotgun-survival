﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Scripts.UI.MiniMap
{  
    public class MinimapBlip : MonoBehaviour
    {
        private Image image;
        private ObjectMap objectMap;
        private MiniMapUI miniMapUI;
        private RectTransform rect;

        private void Awake()
        {
            image = GetComponent<Image>();
            rect = GetComponent<RectTransform>();
        }

        public void Init(MiniMapUI miniMapUI, ObjectMap objectMap)
        {
            this.miniMapUI = miniMapUI;
            this.objectMap = objectMap;
            image.sprite = objectMap.Sprite;
            image.color = objectMap.Color;

            rect.sizeDelta = new Vector2(objectMap.Size, objectMap.Size);
        }

        public bool CheckObjectMap(ObjectMap objectMap)
        {
            return this.objectMap.Equals(objectMap);
        }

        private void LateUpdate()
        {
            if (objectMap == null)
                return;

            Vector2 position = miniMapUI.TransformPosition(objectMap.transform.position);

            if (objectMap.InBound)
               position = miniMapUI.MoveInside(position);

            rect.localScale = new Vector3(miniMapUI.Zoom, miniMapUI.Zoom, 1);

            if (objectMap.UpdateRotation)
                rect.localEulerAngles = miniMapUI.TransformRotation(objectMap.transform.eulerAngles);

            rect.localPosition = position;
        }
    }
}
