﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Scripts.Managers;
public class AIMUI : MonoBehaviour
{
    [SerializeField] private RectTransform[] lines;
    [SerializeField] private float smooth = 10f;
    [SerializeField] private float offset = 10f; 

    private Canvas canvas;
    private float currentOffset;

    private WeaponController weaponController;
    
    private void Awake() 
    {
        SetOffset(0);
        canvas = GetComponentInParent<Canvas>();
    }  

    private void Start() {
        weaponController = PlayerStats.PlayerGO.GetComponent<WeaponController>();
    } 

    private void Update() 
    {
        SetOffset(weaponController.GetScatter());
        foreach(var line in lines)
        {
            Vector3 direction = line.right * currentOffset * canvas.scaleFactor;
            line.position = Vector3.Lerp(line.position, transform.position + direction, smooth * Time.deltaTime);
        }
    }

    public void SetOffset(float value)
    {
        currentOffset = value + offset;
    }
}
