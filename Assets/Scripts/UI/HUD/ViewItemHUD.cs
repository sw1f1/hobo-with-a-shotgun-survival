﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Scripts.Items;
using Scripts.Managers;

public class ViewItemHUD : MonoBehaviour
{
    [SerializeField] private ViewItemUI itemUIPrefab;
    [SerializeField] private float timeAlive = 5f;

    private void Awake()
    {
        EventManager.OnViewItem += AddItemOnShow;
    }

    private void OnDestroy()
    {
        EventManager.OnViewItem -= AddItemOnShow;
    }

    private void AddItemOnShow(FastItem fastItem)
    {
        Item item = ItemManager.GetItem(fastItem);
        var itemUI = Instantiate(itemUIPrefab, transform);
        itemUI.Init(item.Icon, item.Name, fastItem.Count, timeAlive);
    }
}
