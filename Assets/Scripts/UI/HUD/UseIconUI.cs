﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UseIconUI : MonoBehaviour
{
    [SerializeField] private GameObject container;
    [SerializeField] private Text text;
    IUsable obj;

    private void Start()
    {
        EventManager.OnVisibleUseObject += OnVisibleUseObject;
    }

    private void OnDestroy()
    {
        EventManager.OnVisibleUseObject -= OnVisibleUseObject;
    }

    private void OnVisibleUseObject(bool value, IUsable obj)
    {
        this.obj = obj;

        if (obj != null)
        {
            text.text = obj.TextUse;
            transform.position = Camera.main.WorldToScreenPoint(obj.transform.position);
        }

        container.SetActive(value);
    }

    private void Update()
    {
        if (obj == null)
            return;

        Vector3 position = Camera.main.WorldToScreenPoint(obj.transform.position);
        transform.position = Vector3.Lerp(transform.position , position, Time.deltaTime * 10f);
    }
}
