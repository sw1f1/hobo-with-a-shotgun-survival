﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Scripts.Managers;

public class WeaponContainerUI : MonoBehaviour
{
    [SerializeField] private Image itemImage;
    [SerializeField] private Text ammoText;
    [SerializeField] private Color disActiveColor;

    private Image image;

    private void Awake()
    {
        image = GetComponent<Image>();
        SetActiveWeapon(false);
        gameObject.SetActive(false);
    }

    public void UpdateStats(FastItemWeapon fitem, FastItem ammo)
    {
        if (fitem == null || fitem == FastItemWeapon.None)
        {
            gameObject.SetActive(false);
            return;
        }
        else
        {
            gameObject.SetActive(true);
        }

        itemImage.sprite = ItemManager.GetItem(fitem).Icon;

        if(ammo == null)
        {
            ammoText.text = "";
        }else
        {
            ammoText.text = fitem.Clip + " / " + ammo.Count;
        }
    }

    public void SetActiveWeapon(bool value)
    {
        if (value)
        {
            image.color = Color.white;
        }
        else
        {
            image.color = disActiveColor;
        }
    }
}
