﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerWaitUse : MonoBehaviour
{
    [SerializeField] private GameObject container;
    [SerializeField] private Image bar;

    private float timer;
    private IUsable obj;

    private void Start()
    {
        EventManager.OnUsedObject += OnUsedObject;
    }

    private void OnDestroy()
    {
        EventManager.OnUsedObject -= OnUsedObject;
    }

    private void OnUsedObject(IUsable obj)
    {
        this.obj = obj;
        if(obj != null)
        {
            timer = obj.TimeUsed;
            UpdateFillImage();
        }

        container.SetActive(true);
    }

    private void Update()
    {
        if (obj == null)
            return;

        Vector3 position = Camera.main.WorldToScreenPoint(obj.transform.position);
        transform.position = Vector3.Lerp(transform.position, position, Time.deltaTime * 100f);

        timer -= Time.deltaTime;
        UpdateFillImage();

        if(timer <= 0)
        {
            container.SetActive(false);
            obj = null;
        }
    }

    private void UpdateFillImage()
    {
        bar.fillAmount =(obj.TimeUsed - timer) / obj.TimeUsed;
    }
}
