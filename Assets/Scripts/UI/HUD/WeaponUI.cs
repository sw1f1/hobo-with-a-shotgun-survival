﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Scripts.Items;
using Scripts.Managers;

public class WeaponUI : MonoBehaviour
{
    [SerializeField] private WeaponContainerUI[] containers;
    [SerializeField] private int currentWeapon;
    
    private void Start() 
    {
        PlayerStats.PlayerGO.GetComponent<WeaponController>().OnUpdateClip += UpdateStats;
        PlayerStats.Inventory.OnUpdateEquipItem += UpdateWeapon;
        PlayerStats.Inventory.OnSetCurrentWeapon += SetCurrentWeapon;
    }

    private void OnDestroy()
    {
        PlayerStats.Inventory.OnUpdateEquipItem -= UpdateWeapon;
        PlayerStats.Inventory.OnSetCurrentWeapon -= SetCurrentWeapon;
    }


    public void UpdateStats(FastItemWeapon fitem, FastItem ammo)
    {
        containers[currentWeapon].UpdateStats(fitem, ammo);
    }

    public void UpdateWeapon(int index, FastItem fitem)
    {
        //if (index > 1)
         //   return;

        //containers[index].UpdateStats(fitem as FastItemWeapon, null);
    }

    public void SetCurrentWeapon(int index)
    {
        containers[currentWeapon].SetActiveWeapon(false);
        currentWeapon = index;
        containers[currentWeapon].SetActiveWeapon(true);
    }

}
