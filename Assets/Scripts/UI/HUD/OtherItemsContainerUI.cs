﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Scripts.Items;
using Scripts.Managers;

public class OtherItemsContainerUI : MonoBehaviour
{
    [SerializeField] private Image imageItem;
    [SerializeField] private Text count;

    public void Awake()
    {
        gameObject.SetActive(false);
    }

    public void UpdateStats(FastItem fitem)
    {
        if (fitem == null || fitem.Equals(FastItem.None))
        {
            gameObject.SetActive(false);
            return;
        }

        Item item = ItemManager.GetItem(fitem);
        imageItem.sprite = item.Icon;
        count.text = fitem.Count.ToString();

        if(!gameObject.activeSelf)
            gameObject.SetActive(true);
    }
}
