﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Scripts.Managers;

public class PlayerBarUI : MonoBehaviour
{
    [Header("Health")]
    [SerializeField] private Image healthFill;
    [SerializeField] private Text healthText;
    [SerializeField] private Image healthFillOnTime;
    [SerializeField] private float timeHealthChange = 1f;

    [Header("Food")]
    [SerializeField] private Image foodFill;
    [SerializeField] private Text foodText;

    [Header("Water")]
    [SerializeField] private Image waterFill;
    [SerializeField] private Text waterText;

    private void OnEnable()
    {
        PlayerStats.PlayerGO.GetComponent<PlayerHealthController>().Health.OnChange += OnChangeHealth;
        PlayerStats.PlayerGO.GetComponent<PlayerHealthController>().Food.OnChange += OnChangeFood;
        PlayerStats.PlayerGO.GetComponent<PlayerHealthController>().Water.OnChange += OnChangeWater;
    }

    private void Update()
    {
        if(healthFillOnTime != null)
            healthFillOnTime.fillAmount = Mathf.Lerp(healthFillOnTime.fillAmount, healthFill.fillAmount, Time.deltaTime * timeHealthChange);
    }

    public void OnChangeHealth(ValueStats health)
    {
    
        if(healthFill != null)
            healthFill.fillAmount = health.Value / health.MaxValue;

        if (healthText != null)
            healthText.text = System.Convert.ToString(health.Value);
    }

    public void OnChangeFood(ValueStats food)
    {
        if (foodFill != null)
            foodFill.fillAmount = food.Value / food.MaxValue;

        if (foodText != null)
            foodText.text = System.Convert.ToString(food.Value);
    }

    public void OnChangeWater(ValueStats water)
    {
        if (waterFill != null)
            waterFill.fillAmount = water.Value / water.MaxValue;

        if (waterText != null)
            waterText.text = System.Convert.ToString(water.Value);
    }
}
