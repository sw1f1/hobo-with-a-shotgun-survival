﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScopeAIMUI : MonoBehaviour
{
    [SerializeField] private GameObject container;

    private void OnEnable()
    {
        EventManager.OnSetActiveScopeAIM += SetActiveContainer;
    }

    private void OnDisable()
    {
        EventManager.OnSetActiveScopeAIM -= SetActiveContainer;
    }

    public void SetActiveContainer(bool value)
    {
        container.SetActive(value);
    }
}
