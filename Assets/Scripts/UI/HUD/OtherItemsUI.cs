﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Scripts.Items;
using Scripts.Managers;

public class OtherItemsUI : MonoBehaviour
{
    [SerializeField] private OtherItemsContainerUI[] containers;

    private void Start()
    {
        PlayerStats.Inventory.OnUpdateEquipItem += UpdateWeapon;
    }

    private void OnDestroy()
    {
        PlayerStats.Inventory.OnUpdateEquipItem -= UpdateWeapon;
    }


    public void UpdateWeapon(int index, FastItem fitem)
    {
        index -= 2;
        if (index < 0 || index >= containers.Length)
            return;

        containers[index].UpdateStats(fitem);
    }
}

