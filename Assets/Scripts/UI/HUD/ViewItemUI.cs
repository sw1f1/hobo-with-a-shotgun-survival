﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ViewItemUI : MonoBehaviour
{
    [SerializeField] private RectTransform view;
    [SerializeField] private Image image;
    [SerializeField] private Text nameText;
    [SerializeField] private Text countText;

    public void Init(Sprite icon, string name, int count, float timeAlive)
    {
        image.sprite = icon;
        nameText.text = name;
        countText.text = "+ " + count;

        StartCoroutine(Movement(timeAlive));
    }

    private void OnDisable()
    {
        StopAllCoroutines();
        Destroy(gameObject);
    }

    IEnumerator Movement(float timeAlive)
    {
        Vector3 movePoint = new Vector3(500, 0, 0);

        view.localPosition = movePoint;
        while ((view.localPosition - Vector3.zero).magnitude > 0.1f)
        {
            view.localPosition = Vector3.Slerp(view.localPosition, Vector3.zero, Time.deltaTime * 5f);
            yield return null;
        }

        yield return new WaitForSeconds(timeAlive);

        while ((view.localPosition - movePoint).magnitude > 0.1f)
        {
            view.localPosition = Vector3.Slerp(view.localPosition, movePoint, Time.deltaTime * 5f);
            yield return null;
        }

        Destroy(gameObject);
    }
}
