﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Scripts.Items;
using Scripts.Managers;

public interface IDragable
{
    FastItem FastItem {get;}
    Vector3 Position {get;}
    Sprite Icon {get;}
    void SetActiveDrag(bool value);
    void OnToggle(bool value);

    void DestoryItem();
}
