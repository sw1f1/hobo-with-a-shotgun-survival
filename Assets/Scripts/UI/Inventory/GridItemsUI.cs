﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using static Scripts.Items.Item;
using Scripts.Managers;

public class GridItemsUI : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IDropable
{
    [SerializeField] private Transform container;
    [SerializeField] private int startCountItems = 25;
    [SerializeField] private int countRowItems = 5;
    [SerializeField] private ItemUI itemPrefab;
    [SerializeField] private List<ItemUI> items = new List<ItemUI>();

    private IDragable selectedItem;

    private BaseInventory inventory;

    public Action<FastItem> OnSelectItem;
    public Action<FastItem> OnSelectItemForInfo;

    private Outline outline;

    public IDragable SelectedItem => selectedItem;


    private void Awake() 
    {
        outline = GetComponent<Outline>();
        FindAllItemsUI();
    }

    private void OnDisable() 
    {
        ClickOnItem(null);
    }

    public void FindAllItemsUI()
    {
        items.Clear();

        ItemUI[] findItems = gameObject.GetComponentsInChildren<ItemUI>();
        
        foreach(var item in findItems)
        {
            AddItemUI(item);
        }
    }

    private void AddItemUI(ItemUI item)
    {
        item.OnClick += ClickOnItem;
        item.OnDoubleClick += OnDoubleClickItem;
        item.OnLongClick += OnLongClickItem;
        item.OnDrag += OnDragItem;
        item.OnRemove += OnRemoveItem;
        items.Add(item);
    }

    private void RemoveItemUI(ItemUI item)
    {
        item.OnClick -= ClickOnItem;
        item.OnDoubleClick -= OnDoubleClickItem;
        item.OnLongClick -= OnLongClickItem;
        item.OnDrag -= OnDragItem;
        item.OnRemove -= OnRemoveItem;
        items.Remove(item);
    }

    public void CalculateGrid(BaseInventory inventory, TypeItem typeItem = TypeItem.None)
    {
        this.inventory = inventory;

        if(items.Count == 0)
        {
            FindAllItemsUI();
        }

        int index = 0;
        foreach(var fitem in inventory.Items)
        {
            if(index >= items.Count)
            {
                CreateItemsUI();
            }

            if (typeItem == TypeItem.None || (TypeItem)fitem.Type == typeItem || 
                (typeItem == TypeItem.Weapon && (TypeItem)fitem.Type == TypeItem.Ammo))
            {
                items[index].SetItem(fitem);
                index++;
            }
        }

        for (int i = index; i < items.Count; i++)
        {
            items[i].SetDisactive();
        }

        if(index <= startCountItems)
        {
            index = startCountItems;
        }

        if (index <= items.Count)
        {
            int offset = index % countRowItems;
            if (offset == 0) offset = countRowItems;

            RemoveUIItems(items.Count - (index + (countRowItems - offset)));
        }

        OnSelectItem?.Invoke(null);
    }

    public void CreateItemsUI()
    {
        for(int i = 0; i < countRowItems; i++)
        {
            ItemUI item = Instantiate(itemPrefab, Vector3.zero, Quaternion.identity, container);
            AddItemUI(item);
        }
    }

    public void RemoveUIItems(int count)
    {
        for (int i = 0; i < count; i++)
        {
            ItemUI item = items[items.Count - 1];
            RemoveItemUI(item);
            Destroy(item.gameObject);
        }
    }

    public void ClickOnItem(IDragable item) 
    {
        if(selectedItem != null)
            selectedItem.OnToggle(false);

        selectedItem = item;

        if(selectedItem != null)
        {
            selectedItem.OnToggle(true);   
            OnSelectItem?.Invoke(selectedItem.FastItem);
        }
    }

    private void OnDoubleClickItem(IDoubleTap item)
    {
        FastItem fitem = item.FastItem;
        if (inventory.DeleteItem(fitem))
        {
            MainUI.GetMenu<InventoryMenu>().AddItemToOtherInventory(fitem, inventory);
        }

        StartCoroutine(UpdatePanels(0.1f));
    }

    private void OnLongClickItem(IDoubleTap item)
    {
        OnSelectItemForInfo?.Invoke(item.FastItem);
    }

    public void OnDragItem(IDragable item, PointerEventData data)
    {
        ClickOnItem(item); 
        MainUI.GetMenu<InventoryMenu>().DragAndDropItem.SetActiveItem(item, inventory, data);
    }

    public void OnRemoveItem(ItemUI item)
    {
        inventory.DeleteItem(item.FastItem);
    }

    public void OnPointerEnter (PointerEventData data)
    {
        if (DragAndDropItem.IsActive)
        {
            outline.enabled = true;
            MainUI.GetMenu<InventoryMenu>().DragAndDropItem.SetDropable(this);
        }
    }

    public void OnDrop(IDragable item)
    {
        if(selectedItem == item)
        {
            OnPointerExit(null);
            return;
        }

        outline.enabled = false;

        FastItem fitem = item.FastItem;
        if(inventory.AddItem(fitem))
        {
            item.DestoryItem();
        }

        MainUI.GetMenu<InventoryMenu>().DragAndDropItem.SetDisactive();
        StartCoroutine(UpdatePanels(0));
    }

    public void OnPointerExit (PointerEventData data)
    {
        if (DragAndDropItem.IsActive)
        {
            outline.enabled = false;
            MainUI.GetMenu<InventoryMenu>().DragAndDropItem.SetDropable(null);
        }
    }

    private IEnumerator UpdatePanels(float time)
    {
        yield return new WaitForSeconds(time);
        MainUI.GetMenu<InventoryMenu>().UpdatePanel();
    }
}
