﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public interface IDoubleTap: IPointerClickHandler
{
    FastItem FastItem { get; }
    void OnDoubleTap();
}
