﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Scripts.Managers;
using Scripts.Items;
public class InfoItemUI : MonoBehaviour
{
    [SerializeField] private GameObject content;
    [SerializeField] private Image itemImage;
    [SerializeField] private Text itemName;
    [SerializeField] private Text itemDescription;
    [SerializeField] private Text countText;
    [SerializeField] private Image fillBar;

    [SerializeField] private GridItemsUI grid;

    [SerializeField] private bool onLongClick;

    private void OnEnable() 
    {
        if(!onLongClick)
            grid.OnSelectItem += OnSelectItem;
        else
            grid.OnSelectItemForInfo += OnSelectItem;

        content.SetActive(false);
    }

    private void OnDisable() 
    {
        if (!onLongClick)
            grid.OnSelectItem -= OnSelectItem;
        else
            grid.OnSelectItemForInfo -= OnSelectItem;
    }

    public void OnSelectItem(FastItem fastItem)
    {
        if(fastItem == null || fastItem.Equals(FastItem.None))
        {
            content.SetActive(false);
            return;
        }

        Item item = ItemManager.GetItem(fastItem);

        itemImage.sprite = item.Icon;
        itemName.text = item.name;
        itemDescription.text = item.Description;
        countText.text = Convert.ToString(fastItem.Count);

        itemImage.gameObject.SetActive(true);
        itemName.gameObject.SetActive(true);
        itemDescription.gameObject.SetActive(true);

        if(item.Combine)
            countText.gameObject.SetActive(true);
        else
            countText.gameObject.SetActive(false);

        content.SetActive(true);

        if(fastItem as IStatusItem != null && item.VisibleStatus)
        {
            fillBar.transform.parent.gameObject.SetActive(true);
            fillBar.fillAmount = (fastItem as IStatusItem).Status;
        }else
        {
            fillBar.transform.parent.gameObject.SetActive(false);
        }
    }

    public void Close()
    {
        content.SetActive(false);  
    }
}
