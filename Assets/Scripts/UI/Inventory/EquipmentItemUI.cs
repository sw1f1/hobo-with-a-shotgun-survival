﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Scripts.Items;
using Scripts.Managers;
using static Scripts.Items.Item;

public class EquipmentItemUI : MonoBehaviour, IBeginDragHandler, IPointerEnterHandler, IPointerExitHandler, IDropable, IDragable
{
    [SerializeField] private int index;
    [SerializeField] protected Item.TypeItem typeItem;
    [SerializeField] private FastItem fastItem = FastItem.None;
    [SerializeField] private Image itemImage;
    [SerializeField] private Image previewItemImage;
    [SerializeField] private Image fillBar;
    [SerializeField] private Text countText;
    [SerializeField] private Outline outline;
    [SerializeField] private Color dropTrueColor;
    [SerializeField] private Color dropFalseColor;

    private bool isUsed = false;
    private Item item;
    public Action<IDragable, PointerEventData> OnDrag; 
    public FastItem FastItem => fastItem;
    public Vector3 Position => transform.position;
    public Sprite Icon => itemImage.sprite;

    public bool IsUsed => isUsed;

    private void OnEnable() 
    {
        OnDrag += OnDragItem;
    }

    private void OnDisable() 
    {
         OnDrag -= OnDragItem;
    }

    public void SetItem(FastItem fitem)
    {
        if (fitem == null || fitem.Equals(FastItem.None))
        {
            SetDisactive();
            return;
        }

        isUsed = true;
        fastItem = fitem;

        item = ItemManager.GetItem(fitem);
        itemImage.gameObject.SetActive(true);
        itemImage.sprite = item.Icon;

        if(item as Weapon)
        {
            fillBar.transform.parent.gameObject.SetActive(true);
            fillBar.fillAmount = (fitem as FastItemWeapon).Status;
        }else
        {
            fillBar.transform.parent.gameObject.SetActive(false);
        }
            
        if(countText != null)
        if(item.Combine)
        {
            countText.text = fitem.Count.ToString();
            countText.gameObject.SetActive(true);
        }
        else
        {
            countText.text = "";
        }

        previewItemImage.gameObject.SetActive(false);
    }

    public void SetDisactive()
    {
        isUsed = false;
        fastItem = FastItem.None;
        itemImage.gameObject.SetActive(false);

        if(countText != null)
            countText.text = "";

        fillBar.transform.parent.gameObject.SetActive(false);
        OnToggle(false);
        previewItemImage.gameObject.SetActive(true);
    }

    public void OnToggle(bool value)
    {
        outline.effectColor = dropTrueColor;
        outline.enabled = value;
    }

    public void SetActiveDrag(bool value)
    {
        if(!isUsed)
            return;

        itemImage.gameObject.SetActive(!value);

        if(item as Weapon)
            fillBar.transform.parent.gameObject.SetActive(!value);

        if (countText != null)
            countText.gameObject.SetActive(!value);

        OnToggle(!value);
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if(!isUsed)
            return;
        OnDrag?.Invoke(this, eventData);
    }

    public void OnDragItem(IDragable item, PointerEventData data)
    {
        MainUI.GetMenu<InventoryMenu>().DragAndDropItem.SetActiveItem(item, null, data);
    }

    public void OnPointerEnter (PointerEventData data)
    {
        if (DragAndDropItem.IsActive)
        {
            FastItem fitem = MainUI.GetMenu<InventoryMenu>().DragAndDropItem.FastItem;
            if(CheckTypeOnDropItem(fitem))
            {
                outline.effectColor = dropTrueColor;
            }else
            {
                outline.effectColor = dropFalseColor;
            }

            outline.enabled = true;
            MainUI.GetMenu<InventoryMenu>().DragAndDropItem.SetDropable(this);
        }
    }

    public void OnPointerExit (PointerEventData data)
    {
        if (DragAndDropItem.IsActive)
        {
            outline.enabled = false;
            MainUI.GetMenu<InventoryMenu>().DragAndDropItem.SetDropable(null);
        }
    }

    public void OnDrop(IDragable item)
    {
        FastItem fitem = item.FastItem;

        if(!CheckTypeOnDropItem(fitem))
        {
            UpdateData();
            return;
        }

        PlayerInventory inventory = MainUI.GetMenu<InventoryMenu>().PlayerInventory;

        item.DestoryItem();

        if(fastItem != null || fastItem != FastItem.None)
            inventory.AddItem(fastItem);

        inventory.SetEquipItem(index, fitem);

        SetItem(fitem);

        UpdateData();
    }

    public void DestoryItem()
    {
        PlayerInventory inventory = MainUI.GetMenu<InventoryMenu>().PlayerInventory;
        inventory.SetEquipItem(index, FastItem.None);
        SetDisactive();
    }

    private void UpdateData()
    {
        OnPointerExit(null);
        MainUI.GetMenu<InventoryMenu>().DragAndDropItem.SetDisactive();
        MainUI.GetMenu<InventoryMenu>().UpdatePanel();
    }

    public virtual bool CheckTypeOnDropItem(FastItem fitem)
    {
        if((TypeItem) fitem.Type == TypeItem.Health && typeItem == TypeItem.Food)
        {
            return true;
        }

        Item item = ItemManager.GetItem(fitem);
        if (item.IsTrowable)
        {
            return false;
        }

        return (TypeItem)fitem.Type == typeItem;
    }
}
