﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Scripts.Items;
using Scripts.Managers;

public class DragAndDropItem : MonoBehaviour, IDragHandler, IEndDragHandler, IBeginDragHandler
{
    public static bool IsActive = false;
    [SerializeField] private Image itemImage;
    [SerializeField] private Text countText;
    [SerializeField] private Image fillBar;

    [SerializeField] private float smooth = 100f;
    private IDragable currentItem;
    private Vector3 moveTo;
    private bool moveAndEnd = false;

    private BaseInventory inventory;
    private IDropable currentDrop;

    public FastItem FastItem => currentItem.FastItem;
    public BaseInventory Inventory => inventory;

    public void SetActiveItem(IDragable dragable, BaseInventory inventory, PointerEventData data)
    {
        this.inventory = inventory;
        moveAndEnd = false;
        currentItem = dragable;
        itemImage.sprite = dragable.Icon;
        transform.position = data.position;
        OnPointerEnter(data);
        moveTo = transform.position;
        currentItem.SetActiveDrag(true);
        gameObject.SetActive(true);

        Item item = ItemManager.GetItem(dragable.FastItem);
        if(item.Combine)
        {
            countText.text = Convert.ToString(dragable.FastItem.Count);
            countText.gameObject.SetActive(true);
        }else
        {
            countText.gameObject.SetActive(false);
        }

        if(item as Weapon)
        {
            fillBar.transform.parent.gameObject.SetActive(true);
            fillBar.fillAmount = (dragable.FastItem as FastItemWeapon).Status;
        }else
        {
            fillBar.transform.parent.gameObject.SetActive(false);
        }
    }

    public void SetDisactive()
    {
        gameObject.SetActive(false);
        IsActive = false;

        if(currentItem == null)
            return;

        currentItem.SetActiveDrag(false);
        currentItem = null;
    }

    public void MoveTo(Vector3 position, bool moveAndEnd = false)
    {
        moveTo = position;
        this.moveAndEnd = moveAndEnd;
    }

    private void Update() 
    {
        transform.position = Vector3.Lerp(transform.position, moveTo, Time.deltaTime * smooth);
        if(moveAndEnd && (transform.position - moveTo).magnitude < 0.1f)
        {
            SetDisactive();
        }
    }

    public void OnBeginDrag(PointerEventData data)
    {
        MoveTo(data.position);
        GetComponent<CanvasGroup>().blocksRaycasts = false;
        IsActive = true;
    }

    public void OnDrag(PointerEventData data)
    {
        MoveTo(data.position);
    }
    public void OnEndDrag(PointerEventData data)
    {
        GetComponent<CanvasGroup>().blocksRaycasts = true;
        
        if(currentDrop != null)
        {
            currentDrop.OnDrop(currentItem);
        }

        IsActive = false;

        if(currentItem != null)
            MoveTo(currentItem.Position, true);
    }
    
    public void OnPointerEnter (PointerEventData data )
    {
        data.pointerDrag = gameObject;
        OnBeginDrag(data);
    }

    public void SetDropable(IDropable currentDrop)
    {
        this.currentDrop = currentDrop;
    }
}
