﻿using Scripts.Items;
using Scripts.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static Scripts.Items.Item;

public class InventoryMenu : UIMenu
{
    [SerializeField] private GridItemsUI playerInventory;
    [SerializeField] private GridItemsUI otherInventory;

    [Space]
    [SerializeField] private GameObject leftMenu;
    [SerializeField] private GameObject rightMenu;
    [SerializeField] private GameObject otherInventoryMenu;

    [Space]
    [SerializeField] private EquipmentItemUI[] equipments;

    [Space]
    [SerializeField] private Text weigth;
    [SerializeField] private Image weigthBar;

    [Space]
    [SerializeField] private Text playerName;

    [Space]
    [SerializeField] private DragAndDropItem dragAndDropItem;
    [SerializeField] private Button[] buttonsUseItem;

    [Space]
    [SerializeField] private Image[] sortButton;
    [SerializeField] private Color defoultColorSort;
    [SerializeField] private Color useColorSort;

    private TypeItem currentType = TypeItem.None;
    private PlayerInventory player;
    private BaseInventory other;

    public DragAndDropItem DragAndDropItem => dragAndDropItem;

    public PlayerInventory PlayerInventory => player;

    public void Start()
    {
        playerInventory.OnSelectItem += OnSelectedItemOnInventory;
        SetActiveUseItem(false);
    }

    public void OnDestroy()
    {
        playerInventory.OnSelectItem -= OnSelectedItemOnInventory;
    }

    public override void Open()
    {
        base.Open();
        Cursor.lockState = CursorLockMode.None;
    }

    public override void Close()
    {
        base.Close();
        Cursor.lockState = CursorLockMode.Locked;
        player = null;
        other = null;
        SetActiveUseItem(false);
    }

    public void Open(PlayerInventory player)
    {
        this.player = player;
        playerInventory.CalculateGrid(player, currentType);
        UpdatePlayerStats();

        rightMenu.SetActive(true);
        otherInventoryMenu.SetActive(false);
    }

    public void Open(PlayerInventory player, BaseInventory other)
    {
        Open(player);
        this.other = other;
        otherInventory.CalculateGrid(other);

        rightMenu.SetActive(false);
        otherInventoryMenu.SetActive(true);
    }

    private void UpdatePlayerStats()
    {
        weigth.text = player.Weigth() + " / " + player.MaxWeigth;
        weigthBar.fillAmount = (float)player.Weigth() / (float)player.MaxWeigth;
        playerName.text = player.Name;
        UpdateEquipment();
    }

    private void UpdateEquipment()
    {
        for(int i = 0; i < equipments.Length; i++)
        {
            equipments[i].SetItem(player.GetEquipItem(i));
        }
    }

    public override void UpdatePanel()
    {
        base.UpdatePanel();
        playerInventory.CalculateGrid(player, currentType);
        UpdatePlayerStats();

        if(other != null)
        otherInventory.CalculateGrid(other);
        SetActiveUseItem(false);
    }

    public void TakeAll()
    {
        foreach(var item in other.Items)
        {
            player.AddItem(item);   
        }
        
        other.Clear();
        UpdatePanel();
    }

    public void SetActiveUseItem(bool value)
    {
        foreach(var button in buttonsUseItem)
        {
            button.gameObject.SetActive(value);
        }
    }

    public void OnSelectedItemOnInventory(FastItem fastItem)
    {
        if(fastItem == null)
        {
            SetActiveUseItem(false);
        }else
        {
            SetActiveUseItem(true);
        }
    }

    public void SortItemOnInventiry(int index)
    {
        TypeItem type = (TypeItem)index;
        currentType = type;
        UpdatePanel();
    }

    public void SetActiveSortButton(int index)
    {
        foreach(var btn in sortButton)
        {
            btn.color = defoultColorSort;
        }

        sortButton[index].color = useColorSort;
    }

    public void Use()
    {
        var itemUI = playerInventory.SelectedItem;
        Item item = ItemManager.GetItem(itemUI.FastItem);

        for (int i = 0; i < equipments.Length; i++)
        {
            if(equipments[i].CheckTypeOnDropItem(itemUI.FastItem))
            {
                if((TypeItem)itemUI.FastItem.Type == TypeItem.Weapon && i == 0 && equipments[i].IsUsed && !item.IsTrowable)
                {
                    continue;
                }

                equipments[i].OnDrop(itemUI);
                break;
            }
        }
    }

    public void Split()
    {
        if (playerInventory.SelectedItem == null)
            return;

        FastItem fitem = playerInventory.SelectedItem.FastItem;
        Item item = ItemManager.GetItem(fitem);

        if (!item.Combine || fitem.Count == 1)
            return;

        int count = fitem.Count / 2;
        fitem.Count -= count;

        FastItem newfItem = fitem.Clone();
        newfItem.Count = count;

        player.AddItem(newfItem, false);

        UpdatePanel();
    }

    public void Delete()
    {
        MainUI.GetPanel<ChoicePanelUI>().Open("Are you sure want to delete the item?", OnDeleteCurrentItem);
    }

    private void OnDeleteCurrentItem()
    {
        player.DeleteItem(playerInventory.SelectedItem.FastItem);
        UpdatePanel();
    }

    public void AddItemToOtherInventory(FastItem fastItem, BaseInventory currentInventory)
    {
        BaseInventory inventory;
        if(currentInventory == player)
        {
            inventory = other;
        }else
        {
            inventory = player;
        }

        if (inventory != null)
        {
            inventory.AddItem(fastItem);
        }else
        {
            Use();
        }
    }
}
