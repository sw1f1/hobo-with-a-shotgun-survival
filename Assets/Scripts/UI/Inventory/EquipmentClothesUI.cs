﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Scripts.Items;
using Scripts.Managers;

public class EquipmentClothesUI : EquipmentItemUI
{
    [SerializeField] private Clothes.TypeClothes typeClothes;

    public override bool CheckTypeOnDropItem(FastItem fitem)
    {
        if (!(fitem is FastItemClothes))
            return false;
        Clothes clothes = ItemManager.GetItem(fitem) as Clothes;
        return base.CheckTypeOnDropItem(fitem) && clothes.ClothesType == typeClothes;
    }
}
