﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Scripts.Items;
using Scripts.Managers;

public class EquipmentTrowableUI : EquipmentItemUI
{
    public override bool CheckTypeOnDropItem(FastItem fitem)
    {
        Item item = ItemManager.GetItem(fitem);
        return item.IsTrowable;
    }
}
