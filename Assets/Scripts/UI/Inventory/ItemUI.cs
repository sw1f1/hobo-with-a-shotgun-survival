﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Scripts.Items;
using Scripts.Managers;

public class ItemUI : MonoBehaviour, IBeginDragHandler, IDragable, IDoubleTap, IPointerDownHandler, IPointerUpHandler
{
    public Action<ItemUI> OnClick;
    public Action<ItemUI> OnDoubleClick;
    public Action<ItemUI> OnLongClick;
    public Action<ItemUI> OnRemove; 
    public Action<IDragable, PointerEventData> OnDrag; 

    [SerializeField] private FastItem fastItem;
    [SerializeField] private bool isUsed = false;

    [Space]

    [SerializeField] private Image itemImage;
    [SerializeField] private Text countText;
    [SerializeField] private Image fillBar;
    [SerializeField] private Outline outline;
    [SerializeField] private Color defultColor;
    [SerializeField] private Color selectColor;

    private Image imageBG;
    private Toggle toggle;
    private EventTrigger trigger;

    private Item item;

    private bool longTap;
    private float timeUpPointer;
    private float longTapTime = 1f;

    public FastItem FastItem => fastItem;

    public Vector3 Position => transform.position;

    public Sprite Icon => itemImage.sprite;

    private void Awake() 
    {
        imageBG = GetComponent<Image>();
        toggle = GetComponent<Toggle>();
    }

    private void Update()
    {
        if(longTap)
        {
            if(longTapTime + timeUpPointer <= Time.time)
            {
                OnLongTap();
                OnPointerUp(null);
            }
        }
    }

    public void Click()
    {
        OnClick?.Invoke(this);
    }

    public void OnToggle(bool value)
    {
        if(imageBG == null || toggle == null)
            Awake();
            
        outline.enabled = value;
        if(value)
        {
            imageBG.color = selectColor;
        } else
        {
            imageBG.color = defultColor;
        }
    }

    public void SetItem(FastItem fitem)
    {
        isUsed = true;
        fastItem = fitem;

        item = ItemManager.GetItem(fitem);
        itemImage.gameObject.SetActive(true);
        itemImage.sprite = item.Icon;

        if(trigger == null)
            trigger = gameObject.AddComponent<EventTrigger>();

        if(item.Combine)
        {
            countText.text = Convert.ToString(fitem.Count);
            countText.gameObject.SetActive(true);
        }else
        {
            countText.gameObject.SetActive(false);
        }

        if(fitem as IStatusItem != null && item.VisibleStatus)
        {
            fillBar.transform.parent.gameObject.SetActive(true);
            fillBar.fillAmount = (fitem as IStatusItem).Status;
        }else
        {
            fillBar.transform.parent.gameObject.SetActive(false);
        }

        OnToggle(false);
    }

    public void SetDisactive()
    {
        isUsed = false;
        fastItem = FastItem.None;
        itemImage.gameObject.SetActive(false);
        countText.gameObject.SetActive(false);
        fillBar.transform.parent.gameObject.SetActive(false);

        if(trigger != null)
        {
            Destroy(trigger);
        }

        OnToggle(false);
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if(!isUsed)
            return;
        OnDrag?.Invoke(this, eventData);
    }

    public void SetActiveDrag(bool value)
    {
        itemImage.gameObject.SetActive(!value);

        if(item.Combine)
            countText.gameObject.SetActive(!value);

        if(fastItem as IStatusItem != null && item.VisibleStatus)
            fillBar.transform.parent.gameObject.SetActive(!value);
            
        OnToggle(!value);
    }

    public void DestoryItem()
    {
        OnRemove?.Invoke(this);
        SetDisactive();
    }

    public void OnDoubleTap()
    {
        OnDoubleClick?.Invoke(this);
        OnToggle(false);
    }

    public void OnLongTap()
    {
        OnLongClick?.Invoke(this);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (!isUsed)
            return;

        int tap = eventData.clickCount;
        if(tap == 1)
        {
            Click();
        }
        if(tap == 2)
        {
            OnDoubleTap();
            eventData.Reset();
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        longTap = true;
        timeUpPointer = Time.time;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        longTap = false;
        timeUpPointer = 0;
    }
}
