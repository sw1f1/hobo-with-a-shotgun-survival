using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUDUI : UIMenu
{
    [SerializeField] private Transform usedItemPanel;
    [SerializeField] private GridBuildUI buildingItem;

    private void OnEnable()
    {
        EventManager.OnOpenBuildPanel += OnOpenBuildPanel;
        EventManager.OnCloseBuildPanel += OnCloseBuildPanel;
    }

    private void OnDisable()
    {
        EventManager.OnOpenBuildPanel -= OnOpenBuildPanel;
        EventManager.OnCloseBuildPanel -= OnCloseBuildPanel;
    }

    public override void Open()
    {
        base.Open();
        Cursor.lockState = CursorLockMode.Locked;
    }

    public override void Close()
    {
        base.Close();
        Cursor.lockState = CursorLockMode.None;
    }

    public void OnOpenBuildPanel(PlayerInventory inventory)
    {
        usedItemPanel.gameObject.SetActive(false);

        buildingItem.CalculateGrid(inventory);

        buildingItem.transform.parent.gameObject.SetActive(true);
    }

    public void OnCloseBuildPanel()
    {
        buildingItem.transform.parent.gameObject.SetActive(false);
        usedItemPanel.gameObject.SetActive(true);
    }
}
