﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Scripts.Items;
using Scripts.Managers;

public class BuildItemUI : MonoBehaviour
{
    public Action<BuildItemUI> OnClick;

    [SerializeField] private FastItem fastItem;
    [SerializeField] private bool isUsed = false;

    [Space]

    [SerializeField] private Image itemImage;
    [SerializeField] private Text countText;

    private Outline outline;

    private Item item;

    private bool longTap;
    private float timeUpPointer;
    private float longTapTime = 1f;

    public FastItem FastItem => fastItem;

    public bool IsUsed => isUsed;

    public Sprite Icon => itemImage.sprite;

    private void Awake()
    {
        outline = GetComponent<Outline>();
    }

    public void Click()
    {
        OnClick?.Invoke(this);
    }

    public void OnToggle(bool value)
    {
        if (outline == null)
            Awake();

        outline.enabled = value;
    }

    public void SetItem(FastItem fitem)
    {
        isUsed = true;
        fastItem = fitem;

        item = ItemManager.GetItem(fitem);
        itemImage.gameObject.SetActive(true);
        itemImage.sprite = item.Icon;

        if (item.Combine)
        {
            countText.text = Convert.ToString(fitem.Count);
            countText.gameObject.SetActive(true);
        }
        else
        {
            countText.gameObject.SetActive(false);
        }

        OnToggle(false);
    }

    public void SetDisactive()
    {
        isUsed = false;
        fastItem = FastItem.None;
        itemImage.gameObject.SetActive(false);
        countText.gameObject.SetActive(false);

        OnToggle(false);
    }
}
