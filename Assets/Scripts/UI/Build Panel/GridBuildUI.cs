﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static Scripts.Items.Item;
using Scripts.Managers;

public class GridBuildUI : MonoBehaviour
{
    public Action<FastItem> OnSelectItem;

    [SerializeField] private Transform container;
    [SerializeField] private int startCountItems = 8;
    [SerializeField] private BuildItemUI itemPrefab;
    [SerializeField] private List<BuildItemUI> items = new List<BuildItemUI>();

    private BuildItemUI selectedItem;
    private BaseInventory inventory;

    private void OnEnable()
    {
        InputManager.OnScroll += OnScrollBuild;
    }

    private void OnDisable()
    {
        InputManager.OnScroll -= OnScrollBuild;
    }

    public void FindAllItemsUI()
    {
        items.Clear();

        BuildItemUI[] findItems = gameObject.GetComponentsInChildren<BuildItemUI>();

        foreach (var item in findItems)
        {
            AddItemUI(item);
        }
    }

    private void AddItemUI(BuildItemUI item)
    {
        item.OnClick += OnClickBuildItem;
        items.Add(item);
    }

    private void RemoveItemUI(BuildItemUI item)
    {
        item.OnClick -= OnClickBuildItem;
        items.Remove(item);
    }

    private void OnScrollBuild(Vector2 direction)
    {
        var usedItems = items.Where(x => x.IsUsed).ToList();
        int index = usedItems.IndexOf(selectedItem);
        index += UnityEngine.Mathf.FloorToInt(direction.y);

        if (index >= usedItems.Count)
            index = 0;
        if(index < 0)
            index = usedItems.Count - 1;

        BuildItemUI newItem = usedItems[index];
        OnClickBuildItem(newItem);
    }

    public void OnClickBuildItem(BuildItemUI item)
    {
        if (selectedItem != null)
            selectedItem.OnToggle(false);

        selectedItem = item;

        if (selectedItem != null && selectedItem.IsUsed)
        {
            selectedItem.OnToggle(true);
            OnSelectItem?.Invoke(selectedItem.FastItem);
            EventManager.OnSelectBuildItem?.Invoke(selectedItem.FastItem);
        }
    }

    public void CalculateGrid(BaseInventory inventory)
    {
        this.inventory = inventory;
        TypeItem typeItem = TypeItem.Build;

        if (items.Count == 0)
        {
            FindAllItemsUI();
        }

        int index = 0;
        foreach (var fitem in inventory.Items)
        {
            if (index >= items.Count)
            {
                CreateItemsUI();
            }

            if ((TypeItem)fitem.Type == typeItem)
            {
                items[index].SetItem(fitem);
                index++;
            }
        }

        for (int i = index; i < items.Count; i++)
        {
            items[i].SetDisactive();
        }

        if (index <= startCountItems)
        {
            index = startCountItems;
        }

        if (index <= items.Count)
        {
            RemoveUIItems(items.Count - index);
        }

        OnClickBuildItem(items[0]);
    }

    public void CreateItemsUI()
    {
        BuildItemUI item = Instantiate(itemPrefab, Vector3.zero, Quaternion.identity, container);
        AddItemUI(item);
    }

    public void RemoveUIItems(int count)
    {
        for (int i = 0; i < count; i++)
        {
            BuildItemUI item = items[items.Count - 1];
            RemoveItemUI(item);
            Destroy(item.gameObject);
        }
    }
}
