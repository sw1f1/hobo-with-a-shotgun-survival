﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachineAnimatorCharacter : MonoBehaviour
{
    private WeaponController weaponController;

    private void Awake() {
        weaponController = GetComponent<WeaponController>();
    }

    public void OnEndAttack()
    {
        weaponController.NextAttack();
    }

    public void Reload()
    {
         weaponController.Reloaded();
    }

    public void SwitchWeapon()
    {
        weaponController.SwitchWeapon();
    }

    public void TakedWeapon()
    {
        weaponController.TakedWeapon();
    }

    public void OnThrow()
    {
        weaponController.OnThrow();
    }
}
