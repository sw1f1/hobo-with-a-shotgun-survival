﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReloadSMB : StateMachineBehaviour
{
    private StateMachineAnimatorCharacter stateMachine;
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) 
    {
        if(stateMachine == null)
        {
            stateMachine = animator.GetComponent<StateMachineAnimatorCharacter>();
        }
    }
    public override void OnStateUpdate(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex) 
    {
        
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex) 
    {
        stateMachine.Reload();
    }
}
